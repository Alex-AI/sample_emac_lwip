/**
 * @file:
 * This file implements the doubly linked list
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "linked_list.h"
#include "lwip/def.h"

int linked_list_is_empty(linked_list_t *list)
{
    return !(list->head || list->tail);
}

int linked_list_insert(linked_list_t *list, node_t *prev, node_t *node)
{

    if(prev == node)
    {
        return 1;
    }
    else if(linked_list_is_empty(list))
    {
        node->next = NULL;
        node->prev = NULL;

        list->head = node;
        list->tail = node;

        return 1;
    }
    else if( prev == NULL )
    {
        node->next = list->head;
        list->head->prev = node;
        node->prev = NULL;

        list->head = node;

        return 1;
    }
    else if( !prev->next && linked_list_is_tail(list, prev))
    {
        prev->next = node;
        node->prev = prev;
        node->next = NULL;

        list->tail = node;

        return 1;
    }
    else
    {
        prev->next->prev = node;
        node->next = prev->next;
        prev->next = node;
        node->prev = prev;

        return 1;
    }

    return 0;
    
}

int linked_list_is_node_exists(linked_list_t *list, node_t *node)
{
    node_t *iter = list->head;

    while (iter)
    {
        if(iter == node)
            return 1;
        
        iter = iter->next;
    }

    return 0;
}

node_t *linked_list_remove(linked_list_t *list, node_t *node)
{

    if(!node)
    {
        return NULL;
    }
    if(!node->prev && !node->next)
    {
        list->head = NULL;
        list->tail = NULL;
        return NULL;
    }
    else if( !node->prev && linked_list_is_head(list, node))
    {
        
        node->next->prev = NULL;
        list->head = node->next;

        return list->head;
    }
    else if(!node->next && linked_list_is_tail(list, node))
    {
        node->prev->next = NULL;
        list->tail = node->prev;

        return list->tail;
    }
    else 
    {   
        node->prev->next = node->next;
        node->next->prev = node->prev;

        return node->prev;
    }

}



int linked_list_is_head(linked_list_t *list, node_t *node)
{
    return list->head == node;
}

int  linked_list_is_tail(linked_list_t *list, node_t *node)
{
    return list->tail == node;
}