/**
 * @file:
 * This file implements the doubly linked list
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef ELVEES_HDR_LINKED_LIST_H
#define ELVEES_HDR_LINKED_LIST_H

/**
 * Doubly linked node instance
 * @param next pointer to the next buf
 * @param prev pointer to the prev buf
 */
typedef struct _node_t {
    struct _node_t *next;
    struct _node_t *prev;
} node_t;

/**
 * Doubly linked list instance
 * @param head pointer to the head element of the list
 * @param tail pointer to the tail element of the list
 */
typedef struct _linked_list_t {
    node_t *head;
    node_t *tail;
}linked_list_t;

/**
 * Inserts a new node following the specified node
 * @param list pointer to the list
 * @param prev pointer to the specified node
 * @param node pointer to the node to insert after specified node
 * @return "1" if the node have added successfully, "0" otherwise.
 */
int linked_list_insert(linked_list_t *list, node_t *prev, node_t *node);

/**
 * Removes the node from the list
 * @param list pointer to the list
 * @param node pointer to the node you want to remove
 * @return pointer to the previous node from the one being deleted
 */
node_t *linked_list_remove(linked_list_t *list, node_t *node);

/**
 * Returns whether the list is empty: i.e. whether it's size is zero
 * @param list pointer to the list
 * @return "1" if the list's size is 0, "0" otherwise
 */
int  linked_list_is_empty(linked_list_t *list);

/**
 * Returns whether the node is the head
 * @param list pointer to the list
 * @param node pointer to the node you want to check
 * @return "1" if the node is the head, "0" otherwise
 */
int linked_list_is_head(linked_list_t *list, node_t *node);

/**
 * Returns whether the node is the tail
 * @param list pointer to the list
 * @param node pointer to the node you want to check
 * @return "1" if the node is the tail, "0" otherwise
 */
int  linked_list_is_tail(linked_list_t *list, node_t *node);

/**
 * Returns whether the node exists in list
 * @param list pointer to the list
 * @param node pointer to the node you want to check
 * @return "1" if the node exists in list, "0" otherwise
 */
int linked_list_is_node_exists(linked_list_t *list, node_t *node);

#endif
