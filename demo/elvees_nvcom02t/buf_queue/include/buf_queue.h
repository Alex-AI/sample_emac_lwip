/**
 * @file:
 * This file implements the pbuf queue over doubly linked list
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */


#ifndef ELVESS_HDR_BUF_QUEUE_H
#define ELVESS_HDR_BUF_QUEUE_H

#include "linked_list.h"
#include "lwip/pbuf.h"

/**
 * Queue node instance
 * @param node node instance
 * @param p pointer to the pbuf
 */
typedef struct _buf_queue_node_t
{
    node_t node;
    struct pbuf* p;
}buf_queue_node_t;


/**
 * Queue instance
 * @param BUF_QUEUE_SIZE max queue size
 * @param list list instance
 * @param start array of queue nodes in size of BUF_QUEUE_SIZE
 */
#define BUF_QUEUE_SIZE 16
typedef struct _buf_queue_t
{
    linked_list_t list;
    buf_queue_node_t start[BUF_QUEUE_SIZE];
}buf_queue_t;


/**
 * Initializes the queue
 * @param queue a pointer to the queue
 */
void buf_queue_init( buf_queue_t *queue);

/**
 * Inserts the node into this queue 
 * if it is possible to do
 * @param queue a pointer to the queue
 * @param p pointer to the buffer to insert
 */
void buf_queue_offer(buf_queue_t *queue, struct pbuf* p);

/**
 * Inserts the node into this queue
 * if it isn't possible to do, beforehand removes last added one
 * @param queue a pointer to the queue
 * @param p pointer to the buffer to insert
 */
void buf_queue_push_force(buf_queue_t *queue, struct pbuf* p);

/**
 * Retrieves and removes the head of this queue
 * @param queue a pointer to the queue
 * @return pointer to the buffer to retrieve
 */
struct pbuf *buf_queue_get(buf_queue_t *queue);

/**
 * Gets the next node of the queue
 * @param queue a pointer to the queue
 * @param node a pointer to the node
 * @return a pointer to the next node of the queue
 */
buf_queue_node_t *buf_queue_next(buf_queue_t *queue, buf_queue_node_t *node);
/**
 * Returns whether the queue is empty: i.e. whether it's size is zero
 * @param queue a pointer to the queue
 * @return "1" if the queue's size is 0, "0" otherwise
 */
int buf_queue_is_empty(buf_queue_t *queue);

/**
 * Returns whether the queue is full: i.e. whether it's the maximum size.
 * @param queue a pointer to the queue
 * @return "1" if the queue's size is 0, "0" otherwise.
 */
int buf_queue_is_full(buf_queue_t *queue);
#endif
