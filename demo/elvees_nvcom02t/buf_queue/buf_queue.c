/**
 * @file:
 * This file implements the pbuf queue over doubly linked list
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "buf_queue.h"


void buf_queue_init(buf_queue_t *queue)
{

    queue->list.head = queue->list.tail = NULL;
}


void buf_queue_offer(buf_queue_t *queue, struct pbuf* p)
{

    if(!p || buf_queue_is_full(queue) )
        return;

    buf_queue_node_t *node;
    if(! queue->list.tail)
    {
        node = queue->start;
    }
    else
    {
        node  = buf_queue_next(queue, (buf_queue_node_t *)queue->list.tail);
    }

    node->p = p;

    linked_list_insert(&queue->list, queue->list.tail, &node->node);

}

void buf_queue_push_force(buf_queue_t *queue, struct pbuf* p)
{

    if( !p )
        return;

    buf_queue_node_t *node;
    if(! queue->list.tail)
    {
        node = queue->start;
    }
    else 
    {
        if(buf_queue_is_full(queue))
        {
            pbuf_free(buf_queue_get(queue));
        }

        node  = buf_queue_next(queue, (buf_queue_node_t *)queue->list.tail);
    }
    
    node->p = p;

    linked_list_insert(&queue->list, queue->list.tail, &node->node);

}

struct pbuf *buf_queue_get(buf_queue_t *queue)
{

    buf_queue_node_t *node = (buf_queue_node_t *)queue->list.head;
    if(!node)
        return NULL;

    linked_list_remove(&queue->list, queue->list.head);

    return node->p;
}

int buf_queue_is_full(buf_queue_t *queue)
{
    
    return buf_queue_next(queue, (buf_queue_node_t *)queue->list.tail) == (buf_queue_node_t *)queue->list.head;
}

int buf_queue_is_empty(buf_queue_t *queue)
{
    return linked_list_is_empty(&queue->list);
}

buf_queue_node_t *buf_queue_next(buf_queue_t *queue, buf_queue_node_t *node)
{


    node++;
    if(node >= (queue->start + BUF_QUEUE_SIZE))
    {
        node -= BUF_QUEUE_SIZE;
    }

    return node;

}
