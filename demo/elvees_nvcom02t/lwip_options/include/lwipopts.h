/**
 * @file:
 * This file defines options for lwip core and demo program  
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef LWIP_HDR_LWIPOPTS_H
#define LWIP_HDR_LWIPOPTS_H

/* Echo server settings in mainloop */
#define USE_STATIC_IP               0
#define USE_UDP_PROTO               0
#define USE_TCP_PROTO               (!USE_UDP_PROTO)
#define ECHO_SERVER_LISTEN_IP       IP_ADDR_ANY
#define ECHO_SERVER_LISTEN_PORT     45454
#define MAX_MAINLOOP_TIMER_DELAY    2000
#define CPU_IRQ_VECTOR_IN_SDRAM     1

/* Settings for EMAC */
#define EMAC_ETHSRC                 { 0x54, 0xEE, 0x75, 0xB0, 0x78, 0xB9}
#define EMAC_STATION                0
#define EMAC_BROADCAST              1
#define EMAC_GROUP                  1

/* Settings for the lwIP-core */
#define NO_SYS                      1
#define SYS_LIGHTWEIGHT_PROT        0
#define LWIP_RAW                      1
#define LWIP_NETCONN                0
#define LWIP_SOCKET                 0

/* Settings of the memory management module */
#define MEM_LIBC_MALLOC             1
#define MEMP_MEM_MALLOC             1
#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT 1
#define MEM_ALIGNMENT               8
#define MEM_SIZE                    (4 * 1024 * 1024)
#define MEMP_NUM_PBUF               1024
#define MEMP_NUM_UDP_PCB            20
#define MEMP_NUM_TCP_PCB            20
#define MEMP_NUM_TCP_PCB_LISTEN     16
#define MEMP_NUM_TCP_SEG            128
#define MEMP_NUM_REASSDATA          32
#define MEMP_NUM_ARP_QUEUE          10
#define PBUF_POOL_SIZE              512

/* Settings for the netif */
#define DEFAULT_IP                  "169.254.148.145"
#define DEFAULT_NETMASK             "255.255.0.0"
#define DEFAULT_GATEWAY             "169.254.0.0"
#define LWIP_NETIF_STATUS_CALLBACK  1
#define LWIP_NETIF_LINK_CALLBACK    1
#define LWIP_NETIF_HWADDRHINT       1


/* Settings for the ARP */
#define LWIP_ARP                    1
#define ETHARP_TRUST_IP_MAC         0
#define ETH_PAD_SIZE                0

/* Settings for the ipv4 */
#define LWIP_IPV4                   1
#define IP_REASS_MAX_PBUFS          64
#define IP_FRAG_USES_STATIC_BUF     0
#define IP_DEFAULT_TTL              255
#define IP_SOF_BROADCAST            1
#define IP_SOF_BROADCAST_RECV       1
#define LWIP_ICMP                   1
#define LWIP_BROADCAST_PING         1
#define LWIP_MULTICAST_PING         1

/* Enabling the necessary protocols */
#define LWIP_IPV6                   1
#define LWIP_DHCP                   1
#define DHCP_MAX_DISCOVERY_TRIES    5
#define LWIP_UDP                       1
#define LWIP_ICMP                   1

/* Settings for the TCP */
#define LWIP_TCP                       1
#define TCP_WND                     (4 * TCP_MSS)
#define TCP_MSS                     1460
#define TCP_SND_BUF                 (8 * TCP_MSS)
#define TCP_LISTEN_BACKLOG          1
#define LWIP_TCP_KEEPALIVE          1
#define TCP_KEEPIDLE_DEFAULT        10000UL /* Default KEEPALIVE timer in milliseconds*/
#define TCP_KEEPINTVL_DEFAULT       2000UL  /* Default Time between KEEPALIVE probes in milliseconds*/
#define TCP_KEEPCNT_DEFAULT         9U      /* Default Counter for KEEPALIVE probes*/

/* НSettings for the statistics */
#define LWIP_STATS_DISPLAY          0
#define MEM_STATS                   0
#define SYS_STATS                   0
#define MEMP_STATS                  0
#define LINK_STATS                  0

/* Settings for debugging output */
#define LWIP_DEBUG                  1
#define DEBUG_UART_ID               0
#define DEBUG_UART_DEFAULT_BAUDRATE 115200

#define ETHARP_DEBUG                LWIP_DBG_OFF
#define NETIF_DEBUG                 LWIP_DBG_OFF
#define PBUF_DEBUG                  LWIP_DBG_OFF
#define API_LIB_DEBUG               LWIP_DBG_OFF
#define API_MSG_DEBUG               LWIP_DBG_OFF
#define ICMP_DEBUG                  LWIP_DBG_OFF
#define INET_DEBUG                  LWIP_DBG_OFF
#define IP_DEBUG                    LWIP_DBG_OFF
#define IP_REASS_DEBUG              LWIP_DBG_OFF
#define RAW_DEBUG                   LWIP_DBG_OFF
#define MEM_DEBUG                   LWIP_DBG_OFF
#define MEMP_DEBUG                  LWIP_DBG_OFF
#define SYS_DEBUG                   LWIP_DBG_OFF
#define TCP_DEBUG                   LWIP_DBG_OFF
#define TCP_INPUT_DEBUG             LWIP_DBG_OFF
#define TCP_OUTPUT_DEBUG            LWIP_DBG_OFF
#define TCP_RTO_DEBUG               LWIP_DBG_OFF
#define TCP_CWND_DEBUG              LWIP_DBG_OFF
#define TCP_WND_DEBUG               LWIP_DBG_OFF
#define TCP_FR_DEBUG                LWIP_DBG_OFF
#define TCP_QLEN_DEBUG              LWIP_DBG_OFF
#define TCP_RST_DEBUG               LWIP_DBG_OFF
#define UDP_DEBUG                   LWIP_DBG_OFF
#define TCPIP_DEBUG                 LWIP_DBG_OFF
#define DHCP_DEBUG                  LWIP_DBG_OFF


#endif
