/**
 * @file:
 * This file contains settings needed to adapt lwIP for your compiler and machine architecture
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef LWIP_HDR_CC_H
#define LWIP_HDR_CC_H


#include <stdio.h>
#include <stdlib.h>


#define LWIP_ERR_T       int
#define LWIP_NO_STDINT_H 0

/* Printf formatters for data types */
#define U16_F "hu"
#define S16_F "d"
#define X16_F "hx"
#define U32_F "u"
#define S32_F "d"
#define X32_F "x"
#define SZT_F "uz"

/* Byte order */
#define BYTE_ORDER LITTLE_ENDIAN


/* Structure packing */
#define PACK_STRUCT_FIELD(x) x __attribute__((packed))
#define PACK_STRUCT_STRUCT __attribute__((packed))
#define PACK_STRUCT_BEGIN
#define PACK_STRUCT_END


/* Platform specific diagnostic output */
#define REMOVE_PAREN(A) REMOVE_PAREN_HELPER A
#define REMOVE_PAREN_HELPER(...) __VA_ARGS__
#define LWIP_PLATFORM_DIAG(message) do { printf(REMOVE_PAREN(message)); }while(0);
#define LWIP_PLATFORM_ASSERT(...) do { printf(__VA_ARGS__); }while(0);

#endif
