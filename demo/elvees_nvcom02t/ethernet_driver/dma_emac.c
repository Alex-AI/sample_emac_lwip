/**
 * @file:
 * This file implements functions for EMAC_DMA transfers 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "dma_emac.h"
#include "nvcom02t.h"


int dma_emac_write_txfifo (unsigned int physaddr, unsigned int nbytes)
{
    /* Set the address and length for DMA */
    DMA_EMAC_CH(1).IR.data = physaddr;
    DMA_EMAC_CH(1).CP.data = 0;

    DMA_EMAC_CH(1).CSR.data = 0;
    DMA_EMAC_CH(1).CSR.bits.WCX = nbytes-1;
    DMA_EMAC_CH(1).CSR.bits.WN = 15;

    /* Run the DMA */
    DMA_EMAC_CH(1).CSR.bits.RUN = 1;

    /* Wait until the transfer is finished */
    unsigned int count;
    for (count=100000; count>0; count--) {
        if (! (DMA_EMAC_CH(1).CSR.bits.RUN))
            return 1;
    }

    return 0;

}


int dma_emac_read_rxfifo (unsigned int physaddr, unsigned int nbytes)
{
    /* Set the address and length for DMA */
    DMA_EMAC_CH(0).IR.data = physaddr;
    DMA_EMAC_CH(0).CP.data = 0;

    DMA_EMAC_CH(0).CSR.data = 0;
    DMA_EMAC_CH(0).CSR.bits.WCX = nbytes-1;
    DMA_EMAC_CH(0).CSR.bits.WN = 15;

    /* Run the DMA */
    DMA_EMAC_CH(0).CSR.bits.RUN = 1;

    /* Wait until the transfer is finished */
    unsigned int count;
    for (count=100000; count>0; count--) {
        if (! (DMA_EMAC_CH(0).CSR.bits.RUN))
            return 1;
    }

    return 0;
}
