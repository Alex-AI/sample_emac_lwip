/**
 * @file:
 * This file implements main functions for workind with PHY via EMAC's MD_PORT 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef ELVEES_HDR_PHY_H
#define ELVEES_HDR_PHY_H

#include "ks8721bl.h"

#define MD_OP_READ         (1)
#define MD_OP_WRITE        (2)

/**
 * Writes to the PHY register
 * @param phy_addr PHY address
 * @param phyreg_addr PHY register address
 * @param data data to be recorded
 */
void phy_write (unsigned int phy_addr, unsigned int phyreg_addr, unsigned int data);

/**
 * Reads from the PHY register
 * @param phy_addr PHY address
 * @param phyreg_addr PHY register address
 */
unsigned int phy_read (unsigned int phy_addr, unsigned int phyreg_addr);

/**
 * Initializes PHY
 * @param phy_id PHY ID 
 * @param phy_id_mask ID mask 
 * @return PHY address
 */
unsigned int phy_init(unsigned int phy_id, unsigned int phy_id_mask);

/**
 * Resets PHY
 * @param phy_addr PHY address
 */
void phy_reset(unsigned int phy_addr);

/**
 * Starts the procedure for automatically getting environment parameter settings
 * @param phy_addr PHY address
 */
void phy_start_auto_negotiation (unsigned int phy_addr);

/**
 * Switches loopback mode
 * @param phy_addr PHY address
 * @param on if "1" loopback will be switched on, otherwise loopback will be switched off
 */
void phy_set_loopback(unsigned int phy_addr, int on);

/**
 * Returns whether the link status changed
 * @param phy_addr PHY address
 * @return if "1" the link status changed, "0" otherwise
 */
int phy_link_state_changed(unsigned int phy_addr);

/**
 * Returns whether the link is up
 * @param phy_addr PHY address
 * @return if "1" the link is up, "0" otherwise
 */
int phy_link_is_up(unsigned int phy_addr);

#endif
