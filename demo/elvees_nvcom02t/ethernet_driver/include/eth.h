/**
 * @file:
 * This file implements ethernet driver for 1892VM10YA 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef ELVEES_HDR_ETH_H
#define ELVEES_HDR_ETH_H

#include "lwip/netif.h"
#include "buf_queue.h"
#include "lwipopts.h"

/* Maximum frame size */
#define ETHERNET_MTU 1518
/* Minimum frame size */
#define ETH_MIN_FR_LEN 64



/**
 * Sets the unique MAC address of the EMAC controller
 * @param ethsrc unique MAC address
 */
void eth_set_ethsrc(const unsigned char *ethsrc);

/**
 * Gets the unique MAC address of the EMAC controller
 * @param ethsrc unique MAC address
 */
void eth_get_ethsrc(unsigned char *ethsrc);

/**
 * Initializes ethernet driver and linking the driver to the netif interface
 * @param netif pointer to the network interface
 * @return error code
 */
err_t netif_init_eth(struct netif *netif);

/**
 * The interrupt handler for reception of the frame
 */
void eth_rx_intr_handler();

static buf_queue_t rx_buf_queue;
/**
 * Non-blocking getting the next buffer from the qu
 */
void *eth_try_get_rx_buf();

/**
 * Blocking getting the next buffer from the qu
 */
void *eth_force_get_rx_buf();

/**
 * Returns whether the link status changed
 * @return if "1" the link status changed, "0" otherwise
 */
int eth_link_state_changed();

/**
 * Returns whether the link is up
 * @return if "1" the link is up, "0" otherwise
 */
int eth_link_is_up();

#endif





