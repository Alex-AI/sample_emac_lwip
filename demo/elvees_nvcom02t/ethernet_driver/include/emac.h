/**
 * @file:
 * This file implements some functions for configuring EMAC of 1892VM10YA 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef ELVEES_HDR_EMAC_H
#define ELVEES_HDR_EMAC_H

/* 
 * The MDC clock signal must not exceed 2.5 MHz.
 * The value of the divisor must be even
 */
#define MDC_DIVIDER_VALUE(freq) ((freq/2500000)& (~1)) + 2;

#define ETH_ADDR_L(addr) (addr[0]      )|\
                         (addr[1] << 8 )|\
                         (addr[2] << 16)|\
                         (addr[3] << 24);

#define ETH_ADDR_H(addr) (addr[4]     )|\
                         (addr[5] << 8);


/**
 * Initializes EMAC
 * @param cpu_freq CPU frequency
 * @param eth_mtu maximum frame size
 */
void emac_init(unsigned int cpu_freq, unsigned int eth_mtu);

/**
 * Sets the unique MAC address of the EMAC controller
 * @param ucaddr unique MAC address
 */
void emac_set_UCADDR(const unsigned char *ucaddr);

/**
 * Sets the group MAC address of the EMAC controller
 * @param mcaddr group MAC address
 */
void emac_set_MCADDR(const unsigned char *mcaddr);

/**
 * Sets the destination MAC address of the EMAC controller
 * @param daddr destination MAC address
 */
void emac_set_DADDR(const unsigned char *daddr);

/**
 * Gets the unique MAC address of the EMAC controller
 * @param ucaddr unique MAC address
 */
void emac_get_UCADDR(unsigned char *ucaddr);

/**
 * Gets the group MAC address of the EMAC controller
 * @param mcaddr group MAC address
 */
void emac_get_MCADDR(unsigned char *mcaddr);

/**
 * Gets the destination MAC address of the EMAC controller
 * @param daddr destination MAC address
 */
void emac_get_DADDR(unsigned char *daddr);

/**
 * Resets the EMAC controller and FIFO pointers 
 */
void emac_reset_rx_tx();

/**
 * Enables EMAC-LOOPBACK mode
 * @param on on/off
 */
void emac_set_loopback (int on);

/**
 * Sets frame reception permissions by type of destination MAC address
 * @param station permission to receive frames with any destination address
 * @param broadcast permission to receive frames with broadcast destination address
 * @param group permission to receive frames with group destination address
 */
void emac_set_promisc (int station,  int broadcast, int group);



#endif
