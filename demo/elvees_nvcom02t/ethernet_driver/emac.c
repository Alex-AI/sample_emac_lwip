/**
 * @file:
 * This file implements some functions for configuring EMAC of 1892VM10YA
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "emac.h"
#include "nvcom02t.h"
#include "lwipopts.h"

void emac_init(unsigned int cpu_freq, unsigned int eth_mtu)
{
    /* Enable EMAC clock signal */
    SYS_REG.CLK_EN.bits.CLKEN_EMAC = 1;

    /* Set MDC clock signal */
    EMAC.MD_MODE.bits.MDC_DIVIDER = MDC_DIVIDER_VALUE(cpu_freq);

    /* Reset the EMAC controller and FIFO pointers */
    emac_reset_rx_tx();

    /* Set duplex mode*/
    EMAC.CONTROL.bits.FULLD = 1;

    /* Disabling saving the <FCS> field in the received frame */
    EMAC.RX_FRAME_CONTROL.bits.DIS_RCV_FCS = 1;
    /* Set permissions of the reception frame according to the type of MAC address assignment */
    emac_set_promisc ( EMAC_STATION, EMAC_BROADCAST,  EMAC_GROUP);

    /* Enable generation of interrupt after receiving frame */
    EMAC.CONTROL.bits.MASK_RX_DONE = 1;
    /* Enable frame transfer unit */
    EMAC.CONTROL.bits.EN_TX = 1;
    /* Enable frame TX_FIFO DMA unit */
    EMAC.CONTROL.bits.EN_TX_DMA = 1;
    /* Enable frame receiving unit */
    EMAC.CONTROL.bits.EN_RX = 1;

    /* Disable the formation of a frame in the transfer unit */
    EMAC.TX_FRAME_CONTROL.bits.DISENCAPFR = 1;


    /* Processing modes of collisions:
     * Set default values
     * according to the user Manual for 1892VM10YA */
    EMAC.IFS_COLL_MODE.bits.ATTEMPT_NUM = 15;
    EMAC.IFS_COLL_MODE.bits.EN_CW = 1;
    EMAC.IFS_COLL_MODE.bits.COLL_WIN = 64;
    EMAC.IFS_COLL_MODE.bits.JAMB = 0xC3;
    EMAC.IFS_COLL_MODE.bits.IFS = 24;

    /*Maximum frame size */
    EMAC.RX_FR_MAXSIZE.data = eth_mtu;

}

void emac_set_UCADDR(const unsigned char *ucaddr)
{

    EMAC.UCADDR_L.data = ETH_ADDR_L(ucaddr);
    EMAC.UCADDR_H.data = ETH_ADDR_H(ucaddr);
}



void emac_get_UCADDR(unsigned char *ucaddr)
{

    ucaddr[1] = EMAC.UCADDR_L.data >> 8;
    ucaddr[2] = EMAC.UCADDR_L.data >> 16;
    ucaddr[3] = EMAC.UCADDR_L.data >> 24;
    ucaddr[4] = EMAC.UCADDR_H.data;
    ucaddr[5] = EMAC.UCADDR_H.data >> 8;

}


void emac_set_MCADDR(const unsigned char *mcaddr)
{

    EMAC.MCADDR_L.data = ETH_ADDR_L(mcaddr);
    EMAC.MCADDR_H.data = ETH_ADDR_H(mcaddr);
}

void emac_set_DADDR(const unsigned char *daddr)
{

    EMAC.DADDR_L.data = ETH_ADDR_L(daddr);
    EMAC.DADDR_H.data = ETH_ADDR_H(daddr);
}

void emac_reset_rx_tx()
{
    EMAC.CONTROL.bits.CP_TX = 1;
    EMAC.CONTROL.bits.RST_TX = 1;
    EMAC.CONTROL.bits.CP_RX = 1;
    EMAC.CONTROL.bits.RST_RX = 1;
}

void emac_set_loopback ( int on)
{
    EMAC.CONTROL.bits.LOOPBACK = on&&1;
}


void emac_set_promisc ( int station, int broadcast,  int group)
{
    EMAC.RX_FRAME_CONTROL.bits.EN_ALL = station&&1 ;

    EMAC.RX_FRAME_CONTROL.bits.DIS_BC = !broadcast;

    EMAC.RX_FRAME_CONTROL.bits.EN_MCM = group&&1 ;
}



