/**
 * @file:
 * This file implements ethernet driver for 1892VM10YA 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */
#include "eth.h"
#include "lwip/pbuf.h"
#include "dma_emac.h"
#include "emac.h"
#include "irq_handler.h"
#include "phy.h"
#include "lwip/etharp.h"
#include "lwip/ethip6.h"
#include "cpu.h"
#include "nvcom02t.h"
#include "ks8721bl.h"


/* Temporary buffer for sending DMA->TX_FIFO */
static unsigned char tx_buf[ETHERNET_MTU] __attribute__ ((aligned(8)));
/* Address of the PHY device Y */
static unsigned int phy_addr;
/* Queue for receiving incoming frames */
static buf_queue_t rx_buf_queue;

/**
 * Sends the packet to the network
 * @param netif pointer to the network interface
 * @param p the transmitted buffer
 * @return error code
 */
static err_t netif_output(struct netif *netif, struct pbuf *p)
{
    LINK_STATS_INC(link.xmit);

    cpu_lock_interrupts();

    if(!p)
        return ERR_MEM;
    
    pbuf_copy_partial(p, tx_buf, p->tot_len, 0);

    /* According to Ethernet II and IEEE 802.3 standards, any frame less than 64 bytes
     * considered a "collision fragment" and rejected by the receiving station,
     * therefore, the frame is filled with zeros to the minimum size
     */
    unsigned short nbytes = p->tot_len;
    if (nbytes < ETH_MIN_FR_LEN)
    {
        nbytes = ETH_MIN_FR_LEN;
        memset((unsigned char*)tx_buf + p->tot_len, 0, nbytes - p->tot_len);
    }

    /* Writing the frame size to the EMAC register */
    EMAC.TX_FRAME_CONTROL.bits.LENGTH = nbytes ;

    /* DMA-frame transfer to FIFO of EMAC transmitter */
    if(! dma_emac_write_txfifo (virt_to_phys(tx_buf), nbytes  ))
      return ERR_IF;


    /* Creating a transfer request */
    EMAC.TX_FRAME_CONTROL.bits.TX_REQ = 1;

    cpu_unlock_interrupts();
    return ERR_OK;
}


err_t netif_init_eth(struct netif *netif)
{
    /* EMAC initialization */
    unsigned int cpu_freq = cpu_get_freq();
    emac_init( cpu_freq , ETHERNET_MTU);


    /* Setting the source MAC address */
    const unsigned char ethsrc[sizeof(netif->hwaddr)] = EMAC_ETHSRC;
    eth_set_ethsrc(ethsrc);
    SMEMCPY(netif->hwaddr, ethsrc, sizeof(netif->hwaddr));
    netif->hwaddr_len = sizeof(netif->hwaddr);

    /* Initialization of the PHY */
    phy_addr = phy_init(PHY_ID_KS8721BL,  PHY_ID_MASK);

    /* Installing interrupt handler on receiving frame */
    irq_set_handler(ID_INTR_ETH_RX_FRAME, eth_rx_intr_handler);

    netif->linkoutput = netif_output;
    netif->output     = etharp_output;
    netif->output_ip6 = ethip6_output;

    netif->mtu        = ETHERNET_MTU;
    netif->flags      = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_ETHERNET | NETIF_FLAG_IGMP | NETIF_FLAG_MLD6;

    LWIP_PLATFORM_DIAG(("EMAC initialization has been done\n"));
    LWIP_PLATFORM_DIAG(("MAC address is: %x:%x:%x:%x:%x:%x\n",
                            ethsrc[0], ethsrc[1], ethsrc[2],
                            ethsrc[3], ethsrc[4], ethsrc[5]));
    return ERR_OK;
}


void eth_rx_intr_handler()
{
    /* Reading the received frame status */
    union EMAC_RX_FRAME_STATUS_FIFO_REG frame_status;
    frame_status.data  = EMAC.RX_FRAME_STATUS_FIFO.data;

    /* Checking the status */
    if (!frame_status.bits.RECEIVEOK) 
        return;

    /* Reading the packet length field */
    unsigned int rx_fr_length = frame_status.bits.RX_FR_LENGTH;

    /* The frame size is incorrect */
    if (   rx_fr_length > ETHERNET_MTU)
        return;

    /* Service MAC IRQ here */
    /* Allocate pbuf from pool (avoid using heap in interrupts) */
    struct pbuf* p = pbuf_alloc(PBUF_RAW, rx_fr_length, PBUF_POOL);
    if(p != NULL)
    {
        /* Copy ethernet frame into pbuf */
        if(! dma_emac_read_rxfifo(virt_to_phys(p->payload), p->len))
             return;

        /* Forcing a package to be placed in a queue */
        buf_queue_push_force(&rx_buf_queue, p);
    }

    /* Resetting the interrupt request */
    EMAC.STATUS_RX.bits.RX_DONE = 0;
}

void eth_set_ethsrc( const unsigned char *ethsrc)
{
    if(!ethsrc)
        return;
    emac_set_UCADDR(ethsrc);
}

void eth_get_ethsrc( unsigned char *ethsrc)
{
    if(!ethsrc)
        return;
    emac_get_UCADDR(ethsrc);
}

int eth_link_state_changed()
{

    return phy_link_state_changed(phy_addr);
}

int eth_link_is_up()
{
    return phy_link_is_up(phy_addr);
}

void *eth_force_get_rx_buf()
{


    while (buf_queue_is_empty(&rx_buf_queue))
       ;

    return buf_queue_get( &rx_buf_queue);
}

void *eth_try_get_rx_buf()
{

    return buf_queue_get( &rx_buf_queue);
}
