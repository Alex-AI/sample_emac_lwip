/**
 * @file:
 * This file implements main functions for workind with PHY via EMAC MD_PORT 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "phy.h"
#include "nvcom02t.h"


/**
 * PHY register write
 */
void phy_write (unsigned int phy_addr, unsigned int phyreg_addr, unsigned int data)
{

    /* Issue the command to PHY. */
    EMAC.MD_CONTROL.bits.WR_DT = data;
    EMAC.MD_CONTROL.bits.PHY_ADDR = phy_addr;
    EMAC.MD_CONTROL.bits.PHYREG_ADDR = phyreg_addr;
    EMAC.MD_CONTROL.bits.MD_OP = MD_OP_WRITE;

    /* Wait until the PHY write completes. */
    unsigned int i;
    for (i=0; i<100000; ++i) 
    {
        if (! (EMAC.MD_STATUS.bits.MD_BUSY))
            break;
    }

}

/**
 * PHY register read
 */
unsigned int phy_read (unsigned int phy_addr, unsigned int phyreg_addr)
{

    /* Issue the command to PHY. */
    EMAC.MD_CONTROL.bits.PHY_ADDR = phy_addr;
    EMAC.MD_CONTROL.bits.PHYREG_ADDR = phyreg_addr;
    EMAC.MD_CONTROL.bits.MD_OP = MD_OP_READ;

    /* Wait until the PHY read completes. */
    unsigned int i;
    for (i=0; i<100000; ++i) 
    {
        if (! (EMAC.MD_STATUS.bits.MD_BUSY))
            break;
    }

    return EMAC.MD_STATUS.bits.RD_DT;

}


static int link_state;
int phy_link_state_changed(unsigned int phy_addr)
{
    
    int new_link_state = (phy_read(phy_addr, PHY_STS) & PHY_STS_LINK);

    int is_changed = (new_link_state != link_state) ? 1 : 0;

    link_state =  new_link_state;
    
    if(is_changed && link_state)
    {
        phy_start_auto_negotiation(phy_addr);
    }

    return is_changed;
}

int phy_link_is_up(unsigned int phy_addr)
{
   return link_state;
}



unsigned int phy_find_addr(unsigned int phy_id, unsigned int phy_id_mask)
{
    unsigned int id = 0, phy_addr = 0;

    for (phy_addr = 0;phy_addr < 32; phy_addr++)
    {
        id = phy_read (phy_addr, PHY_ID1) << 16 | phy_read (phy_addr, PHY_ID2);
        if (id != 0 && id != 0xffffffff)
            break;
    }

    return ((id & phy_id_mask) == phy_id) ?  phy_addr : 0xffffffff;
}

unsigned int phy_init(unsigned int phy_id, unsigned int phy_id_mask)
{

    /* Reset PHY */
    EMAC.MD_MODE.bits.RST_MD = 1;
    
    /* Find a device address of PHY transceiver.
     * Count up from 0 to 31. */
    unsigned int phy_addr = phy_find_addr(PHY_ID_KS8721BL, PHY_ID_MASK);

    phy_reset(phy_addr);

    /* Enable jabber counter */
    phy_write (
            phy_addr,
            PHY_EXTCTL,
            PHY_EXTCTL_JABBER);


    if(phy_link_is_up(phy_addr))
        phy_start_auto_negotiation(phy_addr);

    return phy_addr;
}


void phy_reset(unsigned int phy_addr)
{
    /* Reset transceiver. */
    unsigned int count;
    phy_write (
            phy_addr,
            PHY_CTL,
            PHY_CTL_RST);
    for (count=10000; count>0; count--)
    {
        if (! (phy_read (phy_addr, PHY_CTL) & PHY_CTL_RST))
            break;
    }

}




void phy_start_auto_negotiation (unsigned int phy_addr)
{
    /*  If auto negotiation unavailable */
    if( ! (phy_read(phy_addr, PHY_STS) & PHY_STS_CAP_ANEG));
        return;
        
    /* Else */
    phy_write (
            phy_addr,
            PHY_ADVRT,
            PHY_ADVRT_CSMA   | PHY_ADVRT_10_HDX  |
            PHY_ADVRT_10_FDX | PHY_ADVRT_100_HDX |
            PHY_ADVRT_100_FDX);

    phy_write (
            phy_addr,
            PHY_CTL,
            PHY_CTL_ANEG_EN | PHY_CTL_ANEG_RST);

}

void phy_set_loopback(unsigned int phy_addr, int on)
{
    unsigned int control;

    /* Set PHY loop-back mode. */
    control = phy_read (phy_addr, PHY_CTL);
    if (on) {
        control |= PHY_CTL_LPBK;
    } else {
        control &= ~PHY_CTL_LPBK;
    }
    phy_write (phy_addr, PHY_CTL, control);
}

