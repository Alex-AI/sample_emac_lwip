/*******************************************************************************
 * nvcom02_uart_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 * Содержит макросы для доступа к регистрам:
 * UART0
 * UART1
 *
 * Пример использования:
 * UART0.THR.data = 0;
 * tmp = UART1.RBR.data;
 *
 ******************************************************************************/
#ifndef NVCOM02_UART_REGS_H_
#define NVCOM02_UART_REGS_H_

//==============================================================================
// IER
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_IER_BITS {     // bits  description
       Uint16  ERBI        :1;    // Разрешение прерывания по наличию принятых данных (RDAI), а также по таймауту (CTI)
       Uint16  ETBEI    :1;    // Разрешение прерывания по отсутствию данных в регистре THR (THREI)
       Uint16  ERLSI    :1;    // Разрешение прерывания по статусу приема данных (RLSI)
       Uint16  EMSI        :1;    // Разрешение прерывания по статусу модема (MSI)
       Uint16  rsrv_1    :4;    // Резерв
};

union UART_IER_REG {
   Uint32                data;
   struct UART_IER_BITS    bits;
};

//==================================================
// IIR
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_IIR_BITS {     // bits  description
       Uint16  IP        :1;    // Признак наличия прерывания
       Uint16  IID        :3;    // Код идентификации прерывания в соответствии
       Uint16  rsrv_1    :2;    // Резерв
       Uint16  FE        :2;    // Признак разрешения работы RCVR и XMIT FIFO
};

union UART_IIR_REG {
   Uint32                    data;
   struct UART_IIR_BITS        bits;
};

//==================================================
// FCR
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_FCR_BITS {     // bits  description
       Uint16  FEWO        :1;        // Разрешение работы XMIT и RCVR FIFO
       Uint16  RFR        :1;        // Установка RCVR FIFO в исходное состояние
       Uint16  TFR        :1;        // Установка XMIT FIFO в исходное состояние
       Uint16  rsrv_1    :3;        // Резерв
       Uint16  RFTL        :2;        // Порог заполнения RCVR FIFO (в байтах), при котором формируется прерывание
};

union UART_FCR_REG {
   Uint32                    data;
   struct UART_FCR_BITS        bits;
};

//==================================================
// LCR
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_LCR_BITS {     // bits  description
       Uint16  WLS    :2;        // Количество бит данных в передаваемом символе
       Uint16  STB    :1;        // Количество стоп-бит
       Uint16  PEN    :1;        // Разрешение генерации (передатчик) или проверки (приемник) контрольного бита
       Uint16  EPS    :1;        // Выбор типа контроля (при PEN=1)
       Uint16  STP    :1;        // Принудительное формирование бита паритета
       Uint16  SBC    :1;        // Формирование обрыва линии
       Uint16  DLAB    :1;        // Управление доступом к регистрам
};

union UART_LCR_REG {
   Uint32                    data;
   struct UART_LCR_BITS        bits;
};

//==================================================
// MCR
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_MCR_BITS {         // bits  description
       Uint16  DTR        :1;    // Управление выходом nDTR
       Uint16  RTS        :1;    // Управление выходом nRTS
       Uint16  OUT1        :1;    // Управление выходом OUT1
       Uint16  OUT2        :1;    // Управление выходом OUT2
       Uint16  LOOP        :1;    // Режим петли
       Uint16  rsrv_1    :3;    // Резерв
};

union UART_MCR_REG {
   Uint32                    data;
   struct UART_MCR_BITS        bits;
};

//==================================================
// LSR
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_LSR_BITS {         // bits  description
       Uint16  RDR    :1;        // Готовность данных
       Uint16  OE    :1;        // Ошибка переполнения
       Uint16  PE    :1;        // Ошибка контрольного бита
       Uint16  FE    :1;        // Ошибка кадра
       Uint16  BI    :1;        // Обрыв линии
       Uint16  THRE    :1;        // Передающий буферный регистр пуст
       Uint16  TEMT    :1;        // Передатчик пуст
       Uint16  EIRF    :1;        // Наличие хотя бы одного признака ошибки в FIFO
};

union UART_LSR_REG {
   Uint32                    data;
   struct UART_LSR_BITS        bits;
};

//==================================================
// MSR
//==================================================
struct __attribute__ ((packed,aligned(4))) UART_MSR_BITS {         // bits  description
       Uint16  DCTS        :1;    // Признаки любого изменения состояния входного сигнала CTS
       Uint16  DDSR        :1;    // Признаки любого изменения состояния входного сигнала DSR
       Uint16  TERI        :1;    // Признаки перехода входного сигнала RI с низкого уровня на высокий уровень
       Uint16  DDCD        :1;    // Признаки любого изменения состояния входного сигнала nDCD
       Uint16  CTS        :1;    // Состояние сигнала на входе nCTS
       Uint16  DSR        :1;    // Состояние сигнала на входе nDSR
       Uint16  RI        :1;    // Состояние сигнала на входе nRI
       Uint16  DCD        :1;    // Состояние сигнала на входе nDCD
};

union UART_MSR_REG {
   Uint32                    data;
   struct UART_MSR_BITS        bits;
};

//==============================================================================
// UART структура
//==============================================================================
typedef volatile struct  {
   union {
      union DATA_REG_COMMON_UNION      RBR;    // 0 (DLAB=0, R), Приемный буферный регистр
      union DATA_REG_COMMON_UNION      THR;    // 0 (DLAB=0, W), Передающий буферный регистр
      union DATA_REG_COMMON_UNION    DLL;    // 0 (DLAB=1) Регистр делителя младший
   };
   union {
       union UART_IER_REG              IER;    // 0x4 (DLAB=0)    Регистр разрешения прерываний
       union DATA_REG_COMMON_UNION    DLM;    // 0x4 (DLAB=1) Регистр делителя старший
   };
   union {
       union UART_IIR_REG              IIR;    // 0x8 (R) Регистр идентификации прерывания
       union UART_FCR_REG              FCR;    // 0x8 (W) Регистр управления FIFO
   };
   union UART_LCR_REG                  LCR;    // 0xC Регистр управления линией
   union DATA_REG_COMMON_UNION        reserv0;// 0x10
// union UART_MCR_REG                  MCR;    // 0x10 Регистр управления модемом
   union {
       union UART_LSR_REG              LSR;    // 0x14 (R) Регистр состояния линии
       union DATA_REG_COMMON_UNION    SCLR;    // 0x14 (W) Регистр предделителя (scaler)
   };
   union DATA_REG_COMMON_UNION        reserv1;// 0x18
// union UART_MSR_REG                MSR;    // 0x18 Регистр состояния модема
   union DATA_REG_COMMON_UNION        SPR;    // 0x1C Регистр Scratch Pad
}UART_REGS_TYPE;

//==================================================
//    Макросы
//==================================================
#define UART0    (*(   (UART_REGS_TYPE*)UART0_ADDR   ))
#define UART1 (*((UART_REGS_TYPE*)UART1_ADDR))
//==============================================================================

#endif /* NVCOM02_UART_REGS_H_ */
