/*******************************************************************************
 * nvcom02_vpin_vpout_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 * Содержит макросы для доступа к регистрам VPIN, VPOUT
 *
 * Пример использования смотри в < nvcom02_dma_mem_ch_regs.h >
 *
 ******************************************************************************/
#ifndef NVCOM02_VPIN_VPOUT_REGS_H_
#define NVCOM02_VPIN_VPOUT_REGS_H_

//==============================================================================
// CSR
//==================================================
struct __attribute__ ((packed,aligned(4))) VPIN_VCSR_BITS {    // bits  description
       Uint16  INT            :1;    // Флаг прерывания
       Uint16  FIFO_FULL    :1;    // Флаг заполненности FIFO
       Uint16  FIFO_ERR        :1;    // Флаг ошибки FIFO
       Uint16  FIFO_EMPTY    :1;    // Флаг пустого FIFO
       Uint16  rsrv_1        :11;// Резерв
       Uint16  FDMA            :1;    // Очистка DMA
       Uint16  ERPM            :2;    // Способ интерпретации входных видеоданных
       Uint16  rsrv_2        :1;    // Резерв
       Uint16  INT_EN        :1;    // CSR[19]=1 – разрешено прерывание по ошибке FIFO
       Uint16  ORPM            :2;    // Способ интерпретации входных видеоданных
       Uint16  MRK            :1;    // MRK=1 - режим декодирования маркера
       Uint16  INT_MSK        :6;    // Маска прерывания
       Uint16  SNAPSHORT    :1;    // Съемка одного кадра
       Uint16  RUN            :1;    // RUN=0 – порт в состоянии останова
       Uint16  CLR            :1;    // Очистка порта
};

union VPIN_VCSR_REG {
    Uint32                    data;
   struct VPIN_VCSR_BITS    bits;
};

//==================================================
// LINE_PIX
//==================================================
struct __attribute__ ((packed,aligned(4))) VPIN_LINE_PIX_BITS {// bits  description
        Uint16  PIX_CNT    :12;       // 12-разрядный счетчик пикселов. Автоматически обнуляется в начале каждой строки
       Uint16  rsrv_1    :4;        // Резерв
       Uint16  LINE_CNT    :12;      // 12-разрядный счетчик строк. Автоматически обнуляется в начале каждого кадра (????????)
       Uint16  rsrv_2    :4;        // Резерв
};

union VPIN_LINE_PIX_REG {
    Uint32                        data;
   struct VPIN_LINE_PIX_BITS    bits;
};

//==================================================
// FRAME_CNTR
//==================================================
struct __attribute__ ((packed,aligned(4))) VPIN_FRAME_CNTR_BITS {     // bits  description
       Uint32  FRAME_CNT:24;// 24-разрядный счетчик кадров
       Uint16  rsrv_1    :2;    // Резерв
       Uint16  H2LINE    :1;    // Текущее состояние сигнала LINE
       Uint16  H2FRAME    :1;    // Текущее состояние сигнала FRAME
       Uint16  DBLERR    :1;    // Двойная ошибка при декодировани маркера (ВТ.656)
       Uint16  H        :1;    // H=0 - активная часть строки (ВТ.656)
       Uint16  V        :1;    // V=0 - активная часть кадра (ВТ.656)
       Uint16  F        :1;    // Поле (ВТ.656)
};

union VPIN_FRAME_CNTR_REG {
    Uint32                        data;
   struct VPIN_FRAME_CNTR_BITS    bits;
};

//==============================================================================
// VPIN структура
//==============================================================================
typedef volatile struct  {
    union VPIN_VCSR_REG         CSR;        // Регистр управления и состояния
    union VPIN_LINE_PIX_REG        LINE_PIX;    // Счетчик строк / счетчик пикселов
    union VPIN_FRAME_CNTR_REG    FRAME_CNTR;    // Счетчик кадров
    union DATA_REG_COMMON_UNION    FIFO_OUT;    // Выход FIFO
}VPIN_REGS_TYPE;

//==================================================
//    Макрос
//==================================================
#define VPIN (*((VPIN_REGS_TYPE*)VPIN_ADDR))
//==============================================================================
// CSR
//==================================================
struct __attribute__((packed,aligned(4))) VPOUT_REGS_HCSR_BITS {         // bits  description
       Uint16  INT            :1;    // Флаг прерывания
       Uint16  FIFO_EMPTY    :1;// Флаг пустого FIFO
       Uint16  FIFO_ERR        :1;    // Флаг ошибки FIFO
       Uint16  rsrv_1        :16;// Резерв
       Uint16  SNAPSHORT    :1;    // Выбор режима Snapshot
       Uint16  EN_VSYNC        :1;    // Бит выбора внутренней/внешней синхронизации VSYNC
       Uint16  EN_HSYNC        :1;    // Бит выбора внутренней/внешней синхронизации HSYNC
       Uint16  EN_VCLKO        :1;    // Бит выбора внутренней/внешней синхронизации VCLKO
       Uint16  INT_MSK        :1;    // Маска прерывания
       Uint16  FEN            :1;    // Разрешение переключения сигнала поля (F)
       Uint16  RUN            :1;    // RUN=0 – порт в состоянии останова
       Uint16  CLR            :1;    // Очистка порта
};

union VPOUT_REGS_HCSR_REG {
    Uint32                        data;
   struct VPOUT_REGS_HCSR_BITS    bits;
};

//==================================================
// DIV
//==================================================
struct  __attribute__((packed,aligned(4)))  VPOUT_DIV_BITS { // bits  description
        Uint16  DIV        :16;    // 16-разрядный целочисленный коэффициент деления системной частоты HCLK
        Uint16  rsrv_1    :16;    // Резерв
};

union VPOUT_DIV_REG {
    Uint32                    data;
   struct VPOUT_DIV_BITS    bits;
};

//==================================================
// HSART_HEND
//==================================================
struct __attribute__ ((packed,aligned(4))) VPOUT_REGS_HSART_HEND_BITS {         // bits  description
       Uint16  HEND        :12;    // Число пикселей в строке
       Uint16  rsrv_1    :4;        // Резерв
       Uint16  HSTART    :12;    // Число пикселей в неактивной части строки
       Uint16  rsrv_2    :4;        // Резерв
};

union VPOUT_REGS_HSART_HEND_REG {
    Uint32                                data;
   struct VPOUT_REGS_HSART_HEND_BITS    bits;
};

//==================================================
// VSART_VEND
//==================================================
struct __attribute__ ((packed,aligned(4))) VPOUT_VSART_VEND_BITS {         // bits  description
       Uint16  VEND        :12;    // Число строк в неактивной части кадра (поля).
       Uint16  rsrv_1    :4;        // Резерв
       Uint16  VSTART    :12;    // Число строк в кадре (поле).
       Uint16  rsrv_2    :4;        // Резерв
};

union VPOUT_VSART_VEND_REG {
    Uint32                            data;
   struct VPOUT_VSART_VEND_BITS        bits;
};

//==================================================
// LINE_PIX
//==================================================
struct __attribute__ ((packed,aligned(4))) VPOUT_LINE_PIX_BITS {     // bits  description
       Uint16  PIX_CNT    :12;    // 12-разрядный счетчик пикселов. Автоматически обнуляется в начале каждой строки.
       Uint16  rsrv_1    :4;        // Резерв
       Uint16  LINE_CNT    :12;    // 12-разрядный счетчик строк. Автоматически обнуляется в начале каждой строки.
       Uint16  rsrv_2    :4;        // Резерв
};

union VPOUT_LINE_PIX_REG {
    Uint32                        data;
   struct VPOUT_LINE_PIX_BITS    bits;
};

//==================================================
// FR_CNTR_OUT
//==================================================
struct __attribute__ ((packed,aligned(4))) VPOUT_FR_CNTR_OUT_BITS { // bits  description
       Uint32  FRAME_CNT    :24;    // 24-разрядный счетчик кадров. Обнуляется при очистке порта (CLR=1).
       Uint16  rsrv_1        :5;        // Резерв
       Uint16  HSYNC        :1;        // Текущее состояние сигнала строчной синхронизации
       Uint16  VSYNC        :1;        // Текущее состояние сигнала кадровой синхронизации
       Uint16  F            :1;        // Поле
};

union VPOUT_FR_CNTR_OUT_REG {
    Uint32                            data;
   struct VPOUT_FR_CNTR_OUT_BITS    bits;
};

//==============================================================================
// VPOUT структура
//==============================================================================
typedef volatile struct  {
    union VPOUT_REGS_HCSR_REG             CSR;        // Регистр управления и состояния
    union VPOUT_DIV_REG                 DIV;        // Регистр периода сигнала VCLKO_out
    union VPOUT_REGS_HSART_HEND_REG     HSART_HEND;    // Регистр начала/конца активной части строки
    union VPOUT_VSART_VEND_REG             VSART_VEND;    // Регистр начала/конца активной части кадра
    union VPOUT_LINE_PIX_REG             LINE_PIX;    // Счетчик строк / счетчик пикселов
    union VPOUT_FR_CNTR_OUT_REG         FR_CNTR_OUT;// Счетчик кадров
    union DATA_REG_COMMON_UNION            RESERV;        // резерв
    union DATA_REG_COMMON_UNION            FIFO_IN;    // Вход FIFO
}VPOUT_REGS_TYPE;

//==================================================
//    Макрос
//==================================================
#define VPOUT (*((VPOUT_REGS_TYPE*)VPOUT_ADDR))
//==============================================================================


#endif /* NVCOM02_VPIN_VPOUT_REGS_H_ */
