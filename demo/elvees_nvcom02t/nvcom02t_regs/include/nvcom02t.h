/*******************************************************************************
 * nvcom02t.h
 *
 *  Created on: 02.04.2014
 *       
 *
 *******************************************************************************
 *
 *             Микросхема 1892ВМ10Я (NVCom-02T)
 *
 *
 *******************************************************************************/
#ifndef NVCOM_02_T_H_
#define NVCOM_02_T_H_

#include "nvcom02_mem_map.h"
#include "nvcom02_dma_mem_ch_regs.h"
#include "nvcom02_dma_port_ch_regs.h"
#include "nvcom02_dsp_regs.h"
#include "nvcom02_emac_regs.h"
#include "nvcom02_mport_regs.h"
#include "nvcom02_i2c_regs.h"
#include "nvcom02_mfbsp_regs.h"
#include "nvcom02_system_regs.h"
#include "nvcom02_timers_regs.h"
#include "nvcom02_uart_regs.h"
#include "nvcom02_vpin_vpout_regs.h"

#endif /* NVCOM_02_T_H_ */
