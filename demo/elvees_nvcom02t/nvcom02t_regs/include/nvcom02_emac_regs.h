/*******************************************************************************
 * nvcom02_emac_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 *
 * Пример использования смотри в << nvcom02_dma_mem_ch_regs.h >>
 *
 ******************************************************************************/
#ifndef NVCOM02_EMAC_REGS_H_
#define NVCOM02_EMAC_REGS_H_

//==============================================================================

//==================================================
// CONTROL
//==================================================
struct __attribute__ ((packed,aligned(4)))  EMAC_CONTROL_BITS {     // bits  description
       Uint16  FULLD        :1;  // Режим работы контроллера
       Uint16  EN_TX_DMA    :1;  // Разрешение работы передающего TX_FIFO с DMА-каналом
       Uint16  EN_TX        :1;  // Разрешение работы блока передачи кадров
       Uint16  MASK_TX_DONE    :1;  // Маска запроса на прерывание от блока передачи кадров
       Uint16  EN_RX        :1;  // Разрешение работы блока приема кадров
       Uint16  LOOPBACK        :1;  // Режим зацикливания блока приема кадров на блок передачи кадров
       Uint16  FULLD_RX        :1;  // Тестовый режим работы блока приема кадров
       Uint16  MASK_RX_DONE    :1;  // Маска запроса прерывания по наличию принятых кадров в принимающем FIFO
       Uint16  MASK_RX_FIFO_OVF_ERR:1; // Маска запроса прерывания по переполнению принимающего FIFO
       Uint16  CP_TX        :1;  // Сброс указателей передающего TX_FIFO
       Uint16  RST_TX        :1;  // Программный сброс блока передачи кадров контроллера
       Uint16  CP_RX        :1;  // Сброс указателей принимающего RX_FIFO
       Uint16  RST_RX        :1;  // Программный сброс блока приема кадров контроллера
};

union EMAC_CONTROL_REG {
   Uint32                    data;
   struct EMAC_CONTROL_BITS    bits;
};

//==================================================
// IFS_COLL_MODE
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_IFS_COLL_MODE_BITS {         // bits  description
       Uint16  ATTEMPT_NUM    :4;    // Максимальное количество попыток повторных передач кадра !!!!!!->4
       Uint16  EN_CW        :1;    // Разрешение отслеживания окна коллизии
       Uint16  rsrv_1        :2;    // Резерв
       Uint16  TM_BACKOFF    :1;    // Включение тестового режима работы блока BACKOFF
       Uint16  COLL_WIN        :8;    // Размер окна коллизии (число переданных байт)
       Uint16  JAMB            :8;    // Значение повторяющегося байта 32-разрядного jam-сообщения
       Uint16  IFS            :8;    // Значение межкадрового интервала – interFrameSpacing – в тактах частоты передачи TX_CLK
};

union EMAC_IFS_COLL_MODE_REG {
   Uint32                            data;
   struct EMAC_IFS_COLL_MODE_BITS    bits;
};

//==================================================
// STATUS_TX
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_STATUS_TX_BITS {         // bits  description
       Uint16  ONTX_REQ        :1;    // Блок передачи кадров выполняет обработку запроса на передачу кадра
       Uint16  ONTRANSM        :1;    // Блок передачи кадров выполняет передачу кадра
       Uint16  BUSY            :1;    // Среда передачи занята – обнаружено наличие несущей
       Uint16  TX_DONE        :1;    // Флаг завершения обработки запроса на передачу кадра
       Uint16  TX_REZ        :5;    // Код результата передачи кадра
       Uint16  rsrv_1        :2;    // Резерв
       Uint16  ONCOL        :1;    // Наличие коллизии в среде передачи
       Uint16  COLL_NUM        :4;    // Счетчик попыток повторных передач кадра
       Uint16  TXW            :10;// Число 64-разрядных слов в передающем TX_FIFO
};

union EMAC_STATUS_TX_REG {
   Uint32                        data;
   struct EMAC_STATUS_TX_BITS    bits;
};

//==================================================
// TX_FRAME_CONTROL
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_TX_FRAME_CONTROL_BITS {         // bits  description
       Uint16  LENGTH        :12;// число байт передаваемого кадра.
       Uint16  TYPE_EN        :1;    // задает в каком качестве используется поле <LENGTH/TYPE> в передаваемом кадре.
       Uint16  FCS_CLT_EN    :1;    // вычисляет блок передачи  кадров при передаче кадра/контрольная сумма CRC32.
       Uint16  DISENCAPFR    :1;    // Запрещает/разрешает режим формирования кадра в блоке передачи кадров.
       Uint16  DISPAD        :1;    // Запрещает/разрешает автоматическое добавление в кадр поля <PAD>.
       Uint16  TX_REQ        :1;    // Запрос на передачу кадра.
};

union EMAC_TX_FRAME_CONTROL_REG {
   Uint32                                data;
   struct EMAC_TX_FRAME_CONTROL_BITS    bits;
};

//==================================================
// RX_FRAME_CONTROL
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_RX_FRAME_CONTROL_BITS {         // bits  description
       Uint16  DIS_RCV_FCS        :1;    // Отключение сохранения поля <FCS> в принятом кадре.
       Uint16  DIS_PAD_DEL        :1;    // Отключение удаления поля <PAD> в принятом кадре.
       Uint16  ACCEPT_TOOSHORT    :1;    // Разрешение приема слишком коротких кадров, размер которых меньше 64 байт.
       Uint16  DISCARD_TOOLONG    :1;    // Разрешение отбрасывания слишком длинных кадров, размер которых больше RX_FR_MaxSize.
       Uint16  DISCARD_FCSCHERR    :1;    // Разрешение отбрасывания кадров с ошибкой проверки контрольной суммы.
       Uint16  DISCARD_LENGTHERR:1;    // Разрешение отбрасывания кадров с ошибкой длины поля данных.
       Uint16  DIS_BC            :1;    // Запрещение приема кадров с широковещательным адресом назначения.
       Uint16  EN_MCM            :1;    // Разрешение приема кадров с групповым адресом назначения, совпадающим с замаскированным групповым адресом назначения.
       Uint16  EN_MCHT            :1;    // Разрешение приема кадров с групповым адресом назначения, разрешенным для приема в хэш-таблице.
       Uint16  EN_ALL            :1;    // Разрешение приема кадров с любым адресом назначения.
};

union EMAC_RX_FRAME_CONTROL_REG {
   Uint32                                data;
   struct EMAC_RX_FRAME_CONTROL_BITS    bits;
};

//==================================================
// STATUS_RX
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_STATUS_RX_BITS {         // bits  description
       Uint16  RCV_DISABLED        :1;    // Прием не разрешен
       Uint16  ONRECEIVE        :1;    // Блок приема кадров выполняет прием кадра
       Uint16  rsrv_1            :1;    // Резерв !!!->1
       Uint16  RX_DONE            :1;    // Флаг наличия принятых кадров в принимающем RX_FIFO
       Uint16  NUM_RX_FR        :7;    // Число принятых кадров
       Uint16  FR_STATUS_OVF_ERR:1;    // Флаг переполнения FIFO статусов принятых кадров
       Uint16  RXW                :10;// Число 64-разрядных слов в принимающем RX_FIFO
       Uint16  rsrv_2            :1;    // Резерв
       Uint16  RX_FIFO_OVF_ERR    :1;    // Флаг переполнения принимающего RX_FIFO
       Uint16  NUM_MISSED_FR    :6;    // Число пропущенных кадров из-за переполнения принимающего RX_FIFO или FIFO статусов принятых кадров.
       //Uint16  rsrv_3            :2;    // Резерв  !!!->2
};

union EMAC_STATUS_RX_REG {
   Uint32                        data;
   struct EMAC_STATUS_RX_BITS    bits;
};

//==================================================
// RX_FRAME_STATUS_FIFO
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_RX_FRAME_STATUS_FIFO_BITS {         // bits  description
       Uint16  RX_FR_LENGTH        :12;// Число байт в принятом кадре
       Uint16  RECEIVEOK        :1;    // Флаг успешного принятия кадра без ошибок
       Uint16  LENGTHERROR        :1;    // Флаг ошибки длины поля данных в принятом кадре
       Uint16  ALIGNMENTERROR    :1; // Флаг ошибки выравнивания в принятом кадре
       Uint16  FRAMECHECKERROR    :1; // Флаг ошибки при проверке принятого кадра
       Uint16  FRAMETOOLONG        :1;    // Флаг принятия слишком длинного кадра
       Uint16  FRAMETOOSHORT    :1;    // Флаг принятия слишком короткого кадра
       Uint16  DRIBBLENIBBLE    :1;    // Флаг поступления нечетного числа полубайт кадра
       Uint16  LEN_FIELD        :1;    // Флаг распознавания поля <LENGTH> в принятом кадре
       Uint16  FCS_Del            :1;    // Флаг удаления поля <FCS> в принятом кадре
       Uint16  PAD_Del            :1;    // Флаг удаления поля <PAD> в принятом кадре
       Uint16  UC                :1;    // Флаг распознавания адреса назначения принятого кадра при совпадении с уникальным адресом MAC.
       Uint16  MCM                :1;    // Флаг распознавания группового адреса при совпадении с замаскированным групповым адресом
       Uint16  MCHT                :1;    // Флаг распознавания группового адреса назначения принятого кадра разрешенного для приема в хэш-таблице
       Uint16  BC                :1;    // Флаг распознавания широковещательного адреса назначения принятого кадра когда разрешен прием кадров с широковещательным адресом назначения.
       Uint16  ALL                :1;    // Флаг распознавания адреса назначения принятого кадра, когда разрешен прием кадров с любым адресом назначения.
       //Uint16  rsrv_1           :5; // Резерв
};

union EMAC_RX_FRAME_STATUS_FIFO_REG {
   Uint32                                    data;
   struct EMAC_RX_FRAME_STATUS_FIFO_BITS    bits;
};

//==================================================
// MD_CONTROL
//==================================================
struct __attribute__((packed,aligned(4))) EMAC_MD_CONTROL_BITS {         // bits  description
       Uint16  WR_DT            :16;// Данные для записи в регистр PHY
       Uint16  PHYREG_ADDR        :5;    // Адрес регистра PHY
       Uint16  rsrv_1            :3;    // Резерв
       Uint16  PHY_ADDR            :5;    // Адрес PHY
       Uint16  MD_MASK            :1;    // Маска запроса на прерывание от порта управления PHY
       Uint16  MD_OP            :2;    // Код выполняемой операции
};

union EMAC_MD_CONTROL_REG {
   Uint32                        data;
   struct EMAC_MD_CONTROL_BITS    bits;
};

//==================================================
// MD_STATUS
//==================================================
struct __attribute__((packed,aligned(4))) EMAC_MD_STATUS_BITS {     // bits  description
       Uint16  RD_DT            :16;// Данные, прочтенные из регистра PHY
       Uint16  rsrv_1            :13;// Резерв
       Uint16  MD_BUSY            :1;    // Признак занятости порта управления PHY – выполняется операция  записи/чтения.
       Uint16  MD_OP_END        :2;    // Флаги завершения выполнения операции
};

union EMAC_MD_STATUS_REG {
   Uint32                        data;
   struct EMAC_MD_STATUS_BITS    bits;
};

//==================================================
// MD_MODE
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_MD_MODE_BITS {     // bits  description
       Uint16  MDC_DIVIDER        :8; // Коэффициент деления системной частоты при формировании частоты MDC.
       Uint16  RST_MD            :1;    // Программный сброс порта управления PHY
};

union EMAC_MD_MODE_REG {
   Uint32                    data;
   struct EMAC_MD_MODE_BITS    bits;
};

//==================================================
// TX_TEST_CSR
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_TX_TEST_CSR_BITS { // bits  description
       Uint16  TM_TX_FIFO    :1;       // Разрешение режима тестирования TX_FIFO
       Uint16  rsrv_1        :3;        // Резерв
       Uint16  TM_TX_RDW    :11;    // Число прочтенных 32-разрядных слов из TX_FIFO в режиме тестирования.
};

union EMAC_TX_TEST_CSR_REG {
   Uint32                        data;
   struct EMAC_TX_TEST_CSR_BITS    bits;
};

//==================================================
// RX_TEST_CSR
//==================================================
struct __attribute__ ((packed,aligned(4))) EMAC_RX_TEST_CSR_BITS {         // bits  description
       Uint16  TM_RX_FIFO    :1;        // Разрешение режима тестирования RX_FIFO
       Uint16  rsrv_1        :3;        // Резерв
       Uint16  TM_RX_RDW    :11;    // Число записанных 32-разрядных слов из RX_FIFO в режиме тестирования.
};

union EMAC_RX_TEST_CSR_REG {
   Uint32                        data;
   struct EMAC_RX_TEST_CSR_BITS    bits;
};

//==============================================================================
// EMAC структура
//==============================================================================
typedef volatile struct  {
   union EMAC_CONTROL_REG         CONTROL;        // Регистр управления MAC
   union DATA_REG_COMMON_UNION     ADDR_L;            // Регистр младшей части исходного адреса MAC
   union DATA_REG_COMMON_UNION     ADDR_H;            // Регистр старшей части исходного адреса MAC
   union DATA_REG_COMMON_UNION     DADDR_L;        // Регистр младшей части адреса назначения
   union DATA_REG_COMMON_UNION     DADDR_H;        // Регистр старшей части адреса назначения
   union DATA_REG_COMMON_UNION     FCS_CLIENT;        // Регистр контрольной суммы кадра
   union DATA_REG_COMMON_UNION     TYPE;            // Регистр типа кадра
   union EMAC_IFS_COLL_MODE_REG IFS_COLL_MODE;    // Регистр IFS и режима обработки коллизии
   union EMAC_TX_FRAME_CONTROL_REG TX_FRAME_CONTROL;// Регистр управления передачи кадра
   union EMAC_STATUS_TX_REG     STATUS_TX;        // Регистр статуса передачи кадра
   union DATA_REG_COMMON_UNION     UCADDR_L;        // Регистр младшей части уникального адреса MAC
   union DATA_REG_COMMON_UNION     UCADDR_H;        // Регистр старшей части уникального адреса MAC
   union DATA_REG_COMMON_UNION     MCADDR_L;        // Регистр младшей части группового адреса
   union DATA_REG_COMMON_UNION     MCADDR_H;        // Регистр старшей части группового адреса
   union DATA_REG_COMMON_UNION     MCADDR_MASK_L;    // Регистр младшей части маски группового адреса
   union DATA_REG_COMMON_UNION     MCADDR_MASK_H;    // Регистр старшей части маски группового адреса
   union DATA_REG_COMMON_UNION     HASHT_L;        // Регистр младшей части хэш-таблицы
   union DATA_REG_COMMON_UNION     HASHT_H;        // Регистр старшей части хэш-таблицы
   union EMAC_RX_FRAME_CONTROL_REG RX_FRAME_CONTROL;    // Регистр управления приема кадра
   union DATA_REG_COMMON_UNION     RX_FR_MAXSIZE;    // Регистр максимального размера принимаемого кадра
   union EMAC_STATUS_RX_REG     STATUS_RX;        // Регистр статуса приема кадра
   union EMAC_RX_FRAME_STATUS_FIFO_REG RX_FRAME_STATUS_FIFO;// FIFO статусов принятых кадров
   union EMAC_MD_CONTROL_REG     MD_CONTROL;        // Регистр управления порта MD
   union EMAC_MD_STATUS_REG     MD_STATUS;        // Регистр статуса порта MD
   union EMAC_MD_MODE_REG         MD_MODE;        // Регистр режима работы порта MD
   union EMAC_TX_TEST_CSR_REG     TX_TEST_CSR;    // Регистр управления и состояния режима тестирования TX_FIFO
   union DATA_REG_COMMON_UNION     TX_FIFO;        // Передающее TX_FIFO
   union EMAC_RX_TEST_CSR_REG     RX_TEST_CSR;    // Регистр управления и состояния режима тестирования  RX_FIFO
   union DATA_REG_COMMON_UNION     RX_FIFO;        // Принимающее RX_FIFO
}EMAC_REGS_TYPE;

//==================================================
//    Макрос
//==================================================
#define EMAC (*((EMAC_REGS_TYPE*)EMAC_ADDR))
//==============================================================================


#endif /* NVCOM02_EMAC_REGS_H_ */
