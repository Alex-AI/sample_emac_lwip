/*******************************************************************************
 * nvcom02_mem_map.h
 *
 *  Created on: 02.04.2014
 *       
 *
 *
 *******************************************************************************
 *
 *             Микросхема 1892ВМ10Я (NVCom-02T)
 *         Адреса устройств для программного доступа.
 *
 ******************************************************************************/

#ifndef NVCOM02_MEM_MAP_H_
#define NVCOM02_MEM_MAP_H_

#define KSEG1_SHIFT            0xa0000000

//==============================================================================
//        CRAM
//==============================================================================
#define CRAM_ADDR                (0x18000000 + KSEG1_SHIFT)
#define CRAM_SIZE                0x1ffff

//==============================================================================
//        CPU
//==============================================================================
#define CPU_REGS_BASE_ADDR        (0x182f0000 + KSEG1_SHIFT)

#define DMA_MEM_CH0_ADDR        (CPU_REGS_BASE_ADDR + 0x0000)
#define DMA_MEM_CH1_ADDR        (CPU_REGS_BASE_ADDR + 0x0080)
#define DMA_MEM_CH2_ADDR        (CPU_REGS_BASE_ADDR + 0x0100)
#define DMA_MEM_CH3_ADDR        (CPU_REGS_BASE_ADDR + 0x0180)

#define DMA_VPIN_ADDR            (CPU_REGS_BASE_ADDR + 0x8800)
#define DMA_VPOUT_ADDR            (CPU_REGS_BASE_ADDR + 0x9800)

#define DMA_EMAC_CH0_ADDR        (CPU_REGS_BASE_ADDR + 0xe800)
#define DMA_EMAC_CH1_ADDR        (CPU_REGS_BASE_ADDR + 0xe840)

#define DMA_MFBSP_RX_CH0_ADDR    (CPU_REGS_BASE_ADDR + 0x7800)
#define DMA_MFBSP_RX_CH1_ADDR    (CPU_REGS_BASE_ADDR + 0x7840)
#define DMA_MFBSP_RX_CH2_ADDR    (CPU_REGS_BASE_ADDR + 0x7880)
#define DMA_MFBSP_RX_CH3_ADDR    (CPU_REGS_BASE_ADDR + 0x78c0)

#define DMA_MFBSP_TX_CH0_ADDR    (CPU_REGS_BASE_ADDR + 0x7c00)
#define DMA_MFBSP_TX_CH1_ADDR    (CPU_REGS_BASE_ADDR + 0x7c40)
#define DMA_MFBSP_TX_CH2_ADDR    (CPU_REGS_BASE_ADDR + 0x7c80)
#define DMA_MFBSP_TX_CH3_ADDR    (CPU_REGS_BASE_ADDR + 0x7cc0)

#define I2C_ADDR                (CPU_REGS_BASE_ADDR + 0x2000)

#define UART0_ADDR                (CPU_REGS_BASE_ADDR + 0x3000)
#define UART1_ADDR                (CPU_REGS_BASE_ADDR + 0x3800)

#define IT0_ADDR                (CPU_REGS_BASE_ADDR + 0xd000)
#define IT1_ADDR                (CPU_REGS_BASE_ADDR + 0xd020)

#define WDT_ADDR                (CPU_REGS_BASE_ADDR + 0xd010)

#define MFBSP0_ADDR                (CPU_REGS_BASE_ADDR + 0x7000)
#define MFBSP1_ADDR                (CPU_REGS_BASE_ADDR + 0x7100)
#define MFBSP2_ADDR                (CPU_REGS_BASE_ADDR + 0x7200)
#define MFBSP3_ADDR                (CPU_REGS_BASE_ADDR + 0x7300)

#define VPIN_ADDR                (CPU_REGS_BASE_ADDR + 0x8000)
#define VPOUT_ADDR                (CPU_REGS_BASE_ADDR + 0x9000)

#define EMAC_ADDR                (CPU_REGS_BASE_ADDR + 0xe000)

#define MCC_ADDR                (CPU_REGS_BASE_ADDR + 0xf000)

#define MPORT_ADDR                (CPU_REGS_BASE_ADDR + 0x1000)

#define SYS_REG_ADDR            (CPU_REGS_BASE_ADDR + 0x4000)

//==============================================================================
//            DSP
//==============================================================================
#define DSP_CORE_ADDR            (0x18400000 + KSEG1_SHIFT)
#define DSP_CORE_SIZE            0x1000

#define DSP0_REGS_BASE_ADDR        (0x18480000 + KSEG1_SHIFT)
#define DSP1_REGS_BASE_ADDR        (0x18880000 + KSEG1_SHIFT)
#define XBUF_BASE_ADDR            (0x187f0000 + KSEG1_SHIFT)

#define DSP0_PRAM_BASE_ADDR       (0x18440000 + KSEG1_SHIFT)
#define DSP1_PRAM_BASE_ADDR       (0x18840000 + KSEG1_SHIFT)
#define DSP_XYRAM0_BASE_ADDR    (0x18400000 + KSEG1_SHIFT)
#define DSP_XYRAM1_BASE_ADDR    (0x18800000 + KSEG1_SHIFT)

#define OFFSET_DSP                 (DSP1_REGS_BASE_ADDR - DSP0_REGS_BASE_ADDR)
#define TOTAL_CNT_DSP            2
//==================================================
//    Адреса блоков регистров
//==================================================
#define DSP_CSR_ADDR            (DSP0_REGS_BASE_ADDR + 0x1000)

#define DSP_XBUF_ADDR            (XBUF_BASE_ADDR + 0xff00)

#define DSP0_PCU_ADDR            (DSP0_REGS_BASE_ADDR + 0x0100)
#define DSP1_PCU_ADDR            (DSP1_REGS_BASE_ADDR + 0x0100)

#define DSP0_ALU_ADDR            (DSP0_REGS_BASE_ADDR + 0x0000)
#define DSP1_ALU_ADDR            (DSP1_REGS_BASE_ADDR + 0x0000)

#define DSP0_AGU_ADDR            (DSP0_REGS_BASE_ADDR + 0x0080)
#define DSP1_AGU_ADDR            (DSP1_REGS_BASE_ADDR + 0x0080)

#define DSP0_RF_ADDR            (DSP0_REGS_BASE_ADDR + 0x0000)
#define DSP1_RF_ADDR            (DSP1_REGS_BASE_ADDR + 0x0000)

#define DSP0_AC_ADDR            (DSP0_REGS_BASE_ADDR + 0x0200)
#define DSP1_AC_ADDR            (DSP1_REGS_BASE_ADDR + 0x0200)

#define DSP0_DB_ADDR            (DSP0_REGS_BASE_ADDR + 0x0500)
#define DSP1_DB_ADDR            (DSP1_REGS_BASE_ADDR + 0x0500)

//==============================================================================
typedef unsigned int          Uint32;
typedef unsigned short        Uint16;
//==============================================================================
union DATA_REG_COMMON_UNION {
   Uint32        data;
};
//==============================================================================
#endif /* NVCOM02_MEM_MAP_H_ */
