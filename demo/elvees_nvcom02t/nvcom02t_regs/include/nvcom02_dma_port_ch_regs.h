/*******************************************************************************
 * nvcom02_dma_port_ch_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 * Содержит следующие макросы для доступа к регистрам :
 * DMA_EMAC_CHn, DMA_EMAC_CH(n) (n=0, 1)
 * DMA_VPIN, DMA_VPOUT
 * DMA_MFBSP_RX_CHn, DMA_MFBSP_RX_CH(n) (n=0...3)
 * DMA_MFBSP_TX_CHn, DMA_MFBSP_TX_CH(n) (n=0...3)
 *
 * Пример использования смотри в << nvcom02_dma_mem_ch_regs.h >>
 *
 ******************************************************************************/
#ifndef NVCOM02_DMA_PORT_CH_REGS_H_
#define NVCOM02_DMA_PORT_CH_REGS_H_

//==============================================================================
struct __attribute__((packed,aligned(4))) DMA_PORT_CH_CSR_BITS {     // bits  description
    Uint16  RUN            :1;    // Состояние работы канала DMA
    Uint16  rsrv_1        :1;    // Резерв
    Uint16  WN            :4;    // Число слов данных (пачка), которое передается за одно предоставление прямого доступа
    Uint16  IPD            :1;    // Запрет прерывания по запросу от порта при выключенном канале DMA (RUN=0).
    Uint16  rsrv_2        :5;    // Резерв
    Uint16  CHEN        :1;    // Разрешение выполнения очередной процедуры    самоинициализации
    Uint16  IM            :1;    // Маска разрешение установки признака END
    Uint16  END            :1;    // Признак завершения передачи блока данных (при IM=1)
    Uint16  DONE        :1;    // Признак завершения передачи блока данных (при CHEN=0)
    Uint16  WCX            :16;   // Число слов данных, которые должен передать канал DMA
};

union DMA_PORT_CH_CSR_REG {
    Uint32                        data;
   struct DMA_PORT_CH_CSR_BITS    bits;
};

//==============================================================================
// DMA_PORT структура
//==============================================================================
typedef volatile struct  {
   union DMA_PORT_CH_CSR_REG     CSR;    // Регистр управления и состояния
   union DATA_REG_COMMON_UNION    CP;        // Регистр указателя цепочки
   union DATA_REG_COMMON_UNION    IR;        // Регистр индекса
   union DMA_PORT_CH_CSR_REG     RUN;    // На запись:Псевдорегистр управления состоянием бита RUN регистра CSR0
                                           // На чтение: Регистр управления и состояния без сброса битов “END” и ”DONE”
}DMA_PORT_CH_REGS_TYPE;

//=================================================================
//    Макросы
//=================================================================
#define DMA_EMAC_CH0 (*((DMA_PORT_CH_REGS_TYPE*)DMA_EMAC_CH0_ADDR))
#define DMA_EMAC_CH1 (*((DMA_PORT_CH_REGS_TYPE*)DMA_EMAC_CH1_ADDR))
//=================================================================
#define DMA_EMAC_CH(num) (*((DMA_PORT_CH_REGS_TYPE*)(DMA_EMAC_CH0_ADDR+0x40*num)))
//==================================================================================
#define DMA_VPIN (*((DMA_PORT_CH_REGS_TYPE*)DMA_VPIN_ADDR))
#define DMA_VPOUT (*((DMA_PORT_CH_REGS_TYPE*)DMA_VPOUT_ADDR))
//==================================================================================
#define DMA_MFBSP_RX_CH0 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_RX_CH0_ADDR))
#define DMA_MFBSP_RX_CH1 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_RX_CH1_ADDR))
#define DMA_MFBSP_RX_CH2 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_RX_CH2_ADDR))
#define DMA_MFBSP_RX_CH3 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_RX_CH3_ADDR))
//========================================================================
#define DMA_MFBSP_RX_CH(num) (*((DMA_PORT_CH_REGS_TYPE*)(DMA_MFBSP_RX_CH0_ADDR+0x40*num)))
//==================================================================================
#define DMA_MFBSP_TX_CH0 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_TX_CH0_ADDR))
#define DMA_MFBSP_TX_CH1 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_TX_CH1_ADDR))
#define DMA_MFBSP_TX_CH2 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_TX_CH2_ADDR))
#define DMA_MFBSP_TX_CH3 (*((DMA_PORT_CH_REGS_TYPE*)DMA_MFBSP_TX_CH3_ADDR))
//========================================================================
#define DMA_MFBSP_TX_CH(num) (*((DMA_PORT_CH_REGS_TYPE*)(DMA_MFBSP_TX_CH0_ADDR+0x40*num)))
//==============================================================================



#endif /* NVCOM02_DMA_PORT_CH_REGS_H_ */
