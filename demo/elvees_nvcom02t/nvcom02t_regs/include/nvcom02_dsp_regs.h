/*******************************************************************************
 * nvcom02_dsp_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *
 *******************************************************************************
 *    Содержит следующие макросы для доступа к группам регистров DSP (n=0, 1):
 * DSP_CSR
 * DSP_XBUF
 * DSPn_PCU
 * DSPn_AGU
 * DSPn_ALU
 * DSPn_RF
 * DSPn_AC
 * DSPn_DB
 *
 * Пример использования:
 * Регистр управления и состояния DCSR, обнуление
 *      DSP0_PCU.DCSR.data = 0;
 * Регистр состояния SR, обнуление
 *        DSP0_PCU.SR.data = 0;
 * Регистр управления и состояния DCSR, обнуление битов BRK
 *         DSP0_PCU.DCSR.bits.BRK = 0;
 ******************************************************************************/
#ifndef NVCOM02_DSP_REGS_H_
#define NVCOM02_DSP_REGS_H_

//==============================================================================
// Регистры управления и состояния (DSP_CSR)
//==============================================================================

//================================================
//    QSTR_DSP
//================================================
struct __attribute__((packed,aligned(4))) DSP_QSTR_BITS {     // bits  description
       Uint16  PI0        :1;  // Программное прерывание DSP0
       Uint16  SE0        :1;  // Прерывание по ошибке стека DSP0
       Uint16  BREAK0    :1;  // Прерывание по останову BREAK DSP0
       Uint16  STP0        :1;  // Прерывание по останову STOP DSP0
       Uint16  rsrv_1    :4;  // Резерв
       Uint16  PI1        :1;  // Программное прерывание DSP1
       Uint16  SE1        :1;  // Прерывание по ошибке стека DSP1
       Uint16  BREAK1    :1;  // Прерывание по останову BREAK DSP1
       Uint16  STP1        :1;  // Прерывание по останову STOP DSP1
       Uint16  rsrv_2    :16; // Резерв
       Uint16  WAIT        :1;  // Прерывание по состоянию ожидания DSP0 – DSP1
       Uint16  rsrv_3    :3;  // Резерв
};
union DSP_QSTR_REG {
   Uint32                data;
   struct DSP_QSTR_BITS    bits;
};
//================================================
//    CSR_DSP
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_XCSR_BITS {     // bits  description
       Uint16  SYNSTART    :1;    // Одновременный старт DSP0 – DSP3
       Uint16  SYNWORK    :1;    // Работа XBUF в синхронном режиме
       Uint16  PMCONFIG    :2;    // Конфигурация программной памяти
       Uint16  rsrv_1    :12;// Резерв
       Uint16  HEN        :1;    // Включение режима определения высокой плотности потоков
       Uint16  DEN        :1;    // Разрешение установки явного приоритета (статический режим)
       Uint16  LEN        :1;    // Бит разрешения ограничителя
       Uint16  rsrv_2    :1;    // Резерв
       Uint16  DPTR        :2;    // Номер ядра, обладающего наивысшим приоритетом
       Uint16  LIMIT    :6;    // Максимальное значение счетчика обращений
       Uint16  rsrv_3    :2;    // Резерв
};
union DSP_XCSR_REG {
   Uint32                data;
   struct DSP_XCSR_BITS    bits;
};
//================================================
// DSP_CSR структура
//================================================
typedef volatile struct  {
    union DSP_QSTR_REG            MASKR_DSP;        // Регистр маски прерываний
    union DSP_QSTR_REG            QSTR_DSP;        // Регистр запросов прерываний
    union DSP_XCSR_REG          CSR_DSP;        // Регистр управления и состояния
    union DATA_REG_COMMON_UNION    TOTAL_CLK_CNTR;    // Счетчик тактов
}DSP_CSR_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_CSR (*((DSP_CSR_REGS_TYPE*)DSP_CSR_ADDR))
#define DSP1_CSR (*((DSP_CSR_REGS_TYPE*)DSP_CSR_ADDR))
#define DSP_CSR (*((DSP_CSR_REGS_TYPE*)DSP_CSR_ADDR))

//==============================================================================
//        Буфер обмена XBUF (DSP_XBUF)
//==============================================================================

struct DSP_LONG_REG {
    union DATA_REG_COMMON_UNION    L;
    union DATA_REG_COMMON_UNION    H;
};
//================================================
// DSP_XBUF структура
//================================================
typedef volatile struct  {
       struct DSP_LONG_REG    X0;        // Регистр обмена X0
       struct DSP_LONG_REG    X1;        // Регистр обмена X1
       struct DSP_LONG_REG    X2;        // Регистр обмена X2
       struct DSP_LONG_REG    X3;        // Регистр обмена X3
       struct DSP_LONG_REG    X4;        // Регистр обмена X4
       struct DSP_LONG_REG    X5;        // Регистр обмена X5
       struct DSP_LONG_REG    X6;        // Регистр обмена X6
       struct DSP_LONG_REG    X7;        // Регистр обмена X7
       struct DSP_LONG_REG    X8;        // Регистр обмена X8
       struct DSP_LONG_REG    X9;        // Регистр обмена X9
       struct DSP_LONG_REG    X10;    // Регистр обмена X10
       struct DSP_LONG_REG    X11;    // Регистр обмена X11
       struct DSP_LONG_REG    X12;    // Регистр обмена X12
       struct DSP_LONG_REG    X13;    // Регистр обмена X13
       struct DSP_LONG_REG    X14;    // Регистр обмена X14
       struct DSP_LONG_REG    X15;    // Регистр обмена X15
       struct DSP_LONG_REG    X16;    // Регистр обмена X16
       struct DSP_LONG_REG    X17;    // Регистр обмена X17
       struct DSP_LONG_REG    X18;    // Регистр обмена X18
       struct DSP_LONG_REG    X19;    // Регистр обмена X19
       struct DSP_LONG_REG    X20;    // Регистр обмена X20
       struct DSP_LONG_REG    X21;    // Регистр обмена X21
       struct DSP_LONG_REG    X22;    // Регистр обмена X22
       struct DSP_LONG_REG    X23;    // Регистр обмена X23
       struct DSP_LONG_REG    X24;    // Регистр обмена X24
       struct DSP_LONG_REG    X25;    // Регистр обмена X25
       struct DSP_LONG_REG    X26;    // Регистр обмена X26
       struct DSP_LONG_REG    X27;    // Регистр обмена X27
       struct DSP_LONG_REG    X28;    // Регистр обмена X28
       struct DSP_LONG_REG    X29;    // Регистр обмена X29
       struct DSP_LONG_REG    X30;    // Регистр обмена X30
       struct DSP_LONG_REG    X31;    // Регистр обмена X31
}DSP_XBUF_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP_XBUF (*((DSP_XBUF_REGS_TYPE*)DSP_XBUF_ADDR))
#define DSP0_XBUF (*((DSP_XBUF_REGS_TYPE*)DSP_XBUF_ADDR))
#define DSP1_XBUF (*((DSP_XBUF_REGS_TYPE*)DSP_XBUF_ADDR))

//==============================================================================
//        Устройство программного управления PCU (DSP_PCU)
//==============================================================================

//================================================
// DCSR
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_DCSR_BITS { // bits  description
       Uint16  PI        :1;        // Программное прерывание
       Uint16  SE        :1;        // Прерывание по ошибке стека
       Uint16  BRK        :1;        // Прерывание по останову BREAK
       Uint16  STP        :1;        // Прерывание по останову STOP
       Uint16  WT        :1;        // состояние ожидания обмена с XBUF
       Uint16  rsrv_1    :9;        // не используется
       Uint16  RUN        :1;        // состояние исполнения программы
       Uint16  rsrv_2    :1;        // не используется
};
union DSP_DCSR_REG {
   Uint32                data;
   struct DSP_DCSR_BITS    bits;
};
//================================================
// SR
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_SR_BITS {         // bits  description
       Uint16  C        :1; // перенос
       Uint16  V        :1;    // признак переполнения
       Uint16  Z        :1;    // признак нулевого результата
       Uint16  N        :1;    // признак отрицательного результата
       Uint16  U        :1;    // признак ненормализованного результата
       Uint16  EV        :1;    // флаг переполнения (с сохранением)
       Uint16  E        :1;    // экспоненциальный признак
       Uint16  T        :1;    // признак истинности последнего условия
       Uint16  rsrv_1    :1;    // не используются
       Uint16  DD        :1;    // управление режимом записи результата
       Uint16  BD        :1;    // управление блокировкой конвейера
       Uint16  YM        :1;    // режим адресации памяти YRAM
       Uint16  rsrv_2    :2;    // не используются
       Uint16  SPLITMODE:2;    // Управление режимом разбиения пересылок
};
union DSP_SR_REG {
   Uint32                data;
   struct DSP_SR_BITS    bits;
};
//================================================
// IRQR
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_IRQR_BITS {     // bits  description
       Uint16  DRQ0        :1; // Запрос на прерывание DSP со стороны канала DMA MemCh0
       Uint16  DRQ1        :1; // Запрос на прерывание DSP со стороны канала DMA MemCh1
       Uint16  DRQ2        :1; // Запрос на прерывание DSP со стороны канала DMA MemCh2
       Uint16  DRQ3        :1; // Запрос на прерывание DSP со стороны канала DMA MemCh3
       Uint32  rsrv_1    :20;// Резерв
       Uint16  IRQ0        :1;    // Запрос на прерывание DSP со стороны DSP0
       Uint16  IRQ1        :1;    // Запрос на прерывание DSP со стороны DSP1
       Uint16  rsrv_2    :2;    // Резерв
       Uint16  INT_TMR    :1;    // Запрос на прерывание DSP со стороны таймера TMR
       Uint16  FPE        :1;    // Исключение при исполении операции в формате плавающей точки (V=1)
       Uint16  QT0        :1;    // Запрос на прерывание DSP со стороны CPU (QSTR0)
       Uint16  QT1        :1;    // Запрос на прерывание DSP со стороны CPU (QSTR1, QSTR2))
};
union DSP_IRQR_REG {
   Uint32                data;
   struct DSP_IRQR_BITS    bits;
};
//================================================
// DSTART
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_DSTART_BITS {     // bits  description
       Uint16  DE0        :1;        // Запрос со стороны DSP на запуск канала DMA MemCh0
       Uint16  DE1        :1;        // Запрос со стороны DSP на запуск канала DMA MemCh1
       Uint16  DE2        :1;        // Запрос со стороны DSP на запуск канала DMA MemCh2
       Uint16  DE3        :1;        // Запрос со стороны DSP на запуск канала DMA MemCh3
       Uint32  rsrv_1    :20;    // Резерв
       Uint16  DSP0        :1;        // Запрос на прерывание DSP0
       Uint16  DSP1        :1;        // Запрос на прерывание DSP1
       Uint16  rsrv_2    :6;        // Резерв
};
union DSP_DSTART_REG {
   Uint32                    data;
   struct DSP_DSTART_BITS    bits;
};
//================================================
// ARBR
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_ARBR_BITS { // bits  description
       Uint16  HEN        :1;        // Включение режима определения высокой плотности потоков
       Uint16  DEN        :1;        // разрешение установки явного приоритета (статический режим)
       Uint16  LEN        :1;        // бит разрешения ограничителя
       Uint16  rsrv_1    :1;        // Резерв
       Uint16  DPTR        :2;        // номер ядра, обладающего наивысшим приоритетом
       Uint16  rsrv_2    :2;        // Резерв
       Uint16  LIMIT    :6;        // максимальное значение счетчика обращений
       Uint16  rsrv_3    :2;        // Резерв
};
union DSP_ARBR_REG {
   Uint32                data;
   struct DSP_ARBR_BITS    bits;
};
//================================================
// LC
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_LC_BITS { // bits  description
       Uint16  NC        :14;       // текущее значение 14-разрядного счетчика программных циклов
       Uint16  LF        :1;        // флаг цикла DO – разряд 14 регистра LC
       Uint16  FV        :1;        // флаг цикла DOFOR – разряд 15 регистра LC
};
union DSP_LC_REG {
   Uint32                data;
   struct DSP_LC_BITS    bits;
};
//================================================
// SP
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_SP_BITS { // bits  description
       Uint16  SP        :4;    // указатель системного стека
       Uint16  SSE        :1;    // флаг ошибки системного стека
       Uint16  UFS        :1;    // флаг переполнения системного стека
       Uint16  rsrv_1    :2;    // не используются
       Uint16  CP        :3;    // указатель стека циклов
       Uint16  CSE        :1;    // флаг ошибки стека циклов
       Uint16  UFC        :1;    // флаг переполнения стека циклов
       Uint16  rsrv_2    :3;    // не используются
};
union DSP_SP_REG {
   Uint32                data;
   struct DSP_SP_BITS    bits;
};
//================================================
// CCR
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_CCR_BITS { // bits  description
       Uint16  C    :1;       // перенос
       Uint16  V    :1;    // признак переполнения
       Uint16  Z    :1;    // признак нулевого результата
       Uint16  N    :1;    // признак отрицательного результата
       Uint16  U    :1;    // признак ненормализованного результата
       Uint16  EV    :1;    // флаг переполнения (с сохранением)
       Uint16  E    :1;    // экспоненциальный признак
       Uint16  T    :1;    // признак истинности последнего условия
       Uint16  S    :1;    // бит включения режима насыщения результата
       Uint16  RND    :1;    // бит управления режимом округления результата
       Uint16  CM    :1;    // признак переноса сформированного в результате выполнения операции OP2
       Uint16  VM    :1;    // признак переполнения результата операции OP2
       Uint16  ZM    :1;    // наличие нулевого результата операции OP2
       Uint16  NM    :1;    // значение знака результата операции OP2
       Uint16  UM    :1;    // признак ненормализованного результата операции OP2
       Uint16  EVM    :1;    // запомненный ранее возникший признак переполнения результата операции OP2
};
union DSP_CCR_REG {
   Uint32                data;
   struct DSP_CCR_BITS    bits;
};
//================================================
// PDNR
//================================================
struct __attribute__ ((packed,aligned(4))) DSP_PDNR_BITS {  // bits  description
       Uint16  CPDN        :5; // текущий код PDN
       Uint16  F        :1; // (X/L) – формат анализируемой информации
       Uint16  rsrv_1    :1;    // не используются
       Uint16  EPDN        :1; // программный признак разрешения детектирования и изменения PDN
       Uint16  SC        :2; // величина масштабирования результата
       Uint16  rsrv_2    :5;    // не используются
       Uint16  ESC        :1; // признак разрешения масштабирования результата
};
union DSP_PDNR_REG {
   Uint32                data;
   struct DSP_PDNR_BITS    bits;
};
//================================================
// DSP_PCU структура
//================================================
typedef volatile struct  {
   union DSP_DCSR_REG              DCSR;        // Регистр режима работы
   union DSP_SR_REG              SR;            // Регистр состояния
   union DATA_REG_COMMON_UNION    IDR;        // Регистр-идентификатор
   union {
      union DATA_REG_COMMON_UNION     EFR;    // Регистр флагов обмена
      union DSP_DSTART_REG          DSTART;    // Регистр запуска DMA со стороны DSP и запросов на прерывания других DSP
   };
   union DSP_IRQR_REG            IRQR;        // Регистр запросов на прерывание DSP
   union DATA_REG_COMMON_UNION    IMASKR;        // Регистр маски запросов на прерывания DSP
   union DATA_REG_COMMON_UNION    TMR;        // Регистр таймера DSP
   union DSP_ARBR_REG            ARBR;        // Регистр управления арбитром памяти DSP
   union DATA_REG_COMMON_UNION  PC;            // Программный счетчик
   union DATA_REG_COMMON_UNION  SS;            // Стек программного счетчика
   union DATA_REG_COMMON_UNION    LA;            // Регистр адреса цикла
   union DATA_REG_COMMON_UNION  CSL;        // Стек адреса цикла
   union DSP_LC_REG                LC;            // Счетчик циклов
   union DATA_REG_COMMON_UNION    CSH;        // Стек счетчика циклов
   union DSP_SP_REG                  SP;            // Регистр указателя стека
   union DATA_REG_COMMON_UNION    SAR;        // Регистр адреса останова 0
   union DATA_REG_COMMON_UNION    CNTR;        // Счетчик исполненных команд
   union DATA_REG_COMMON_UNION  SAR1;        // Регистр адреса останова 1
   union DATA_REG_COMMON_UNION  SAR2;        // Регистр адреса останова 2
   union DATA_REG_COMMON_UNION  SAR3;        // Регистр адреса останова 3
   union DATA_REG_COMMON_UNION  SAR4;        // Регистр адреса останова 4
   union DATA_REG_COMMON_UNION  SAR5;        // Регистр адреса останова 5
   union DATA_REG_COMMON_UNION  SAR6;        // Регистр адреса останова 6
   union DATA_REG_COMMON_UNION  SAR7;        // 0x15C Регистр адреса останова 7

   union DATA_REG_COMMON_UNION    reserv0;    // 0x160 Регистр кодов условий
   union DATA_REG_COMMON_UNION    reserv1;    // 0x164 Регистр параметра денормализации

   union DATA_REG_COMMON_UNION    SFR;        // 0x168 Регистр специальных функций

   union DATA_REG_COMMON_UNION     reserv2;    // 0x16C Резерв

   union DATA_REG_COMMON_UNION  QMASKR0;    // 0x170 Регистр маски запросов на прерывание со стороны CPU (QSTR0)
   union DATA_REG_COMMON_UNION  QMASKR1;    // Регистр маски запросов на прерывание со стороны CPU (QSTR1)
   union DATA_REG_COMMON_UNION  QMASKR2;    // Регистр маски запросов на прерывание со стороны CPU (QSTR2)
}DSP_PCU_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_PCU (*((DSP_PCU_REGS_TYPE*)DSP0_PCU_ADDR))
#define DSP1_PCU (*((DSP_PCU_REGS_TYPE*)DSP1_PCU_ADDR))

//==============================================================================
//        Регистры состояния ALU (DSP_ALU)
//==============================================================================

//================================================
// DSP_ALU структура
//================================================
typedef volatile struct  {
    union DATA_REG_COMMON_UNION    R0_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R2_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R4_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R6_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R8_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R10_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R12_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R14_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R16_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R18_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R20_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R22_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R24_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R26_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R28_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R30_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R1_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R3_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R5_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R7_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R9_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R11_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R13_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R15_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R17_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R19_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R21_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R23_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R25_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R27_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R29_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R31_L;            // 0x7C Регистр данных

    int reserv56_0[56];                        // Резерв

    union DSP_CCR_REG          CCR;            // 0x160 Регистр кодов условий
    union DSP_PDNR_REG      PDNR;            // 0x164 Регистр параметра денормализации

    int reserv6_1[6];                        // Резерв

    struct DSP_LONG_REG    R1_D;    // 0x180 Регистр данных
    struct DSP_LONG_REG    R3_D;    // Регистр данных
    struct DSP_LONG_REG    R5_D;    // Регистр данных
    struct DSP_LONG_REG    R7_D;    // Регистр данных
    struct DSP_LONG_REG    R9_D;    // Регистр данных
    struct DSP_LONG_REG    R11_D;    // Регистр данных
    struct DSP_LONG_REG    R13_D;    // Регистр данных
    struct DSP_LONG_REG    R15_D;    // Регистр данных
    struct DSP_LONG_REG    R17_D;    // Регистр данных
    struct DSP_LONG_REG    R19_D;    // Регистр данных
    struct DSP_LONG_REG    R21_D;    // Регистр данных
    struct DSP_LONG_REG    R23_D;    // Регистр данных
    struct DSP_LONG_REG    R25_D;    // Регистр данных
    struct DSP_LONG_REG    R27_D;    // Регистр данных
    struct DSP_LONG_REG    R29_D;    // Регистр данных
    struct DSP_LONG_REG    R31_D;    // 0x1F8 Регистр данных

    union DATA_REG_COMMON_UNION      AC0_L;    // 0x200 Регистр-аккумулятор АС0
    union DATA_REG_COMMON_UNION      AC1_L;    // Регистр-аккумулятор АС1
    union DATA_REG_COMMON_UNION      AC2_L;    // Регистр-аккумулятор АС2
    union DATA_REG_COMMON_UNION      AC3_L;    // Регистр-аккумулятор АС3
    union DATA_REG_COMMON_UNION      AC4_L;    // Регистр-аккумулятор АС4
    union DATA_REG_COMMON_UNION      AC5_L;    // Регистр-аккумулятор АС5
    union DATA_REG_COMMON_UNION      AC6_L;    // Регистр-аккумулятор АС6
    union DATA_REG_COMMON_UNION      AC7_L;    // Регистр-аккумулятор АС7
    union DATA_REG_COMMON_UNION      AC8_L;    // Регистр-аккумулятор АС8
    union DATA_REG_COMMON_UNION      AC9_L;    // Регистр-аккумулятор АС9
    union DATA_REG_COMMON_UNION      AC10_L;    // Регистр-аккумулятор АС10
    union DATA_REG_COMMON_UNION      AC11_L;    // Регистр-аккумулятор АС11
    union DATA_REG_COMMON_UNION      AC12_L;    // Регистр-аккумулятор АС12
    union DATA_REG_COMMON_UNION      AC13_L;    // Регистр-аккумулятор АС13
    union DATA_REG_COMMON_UNION      AC14_L;    // Регистр-аккумулятор АС14
    union DATA_REG_COMMON_UNION      AC15_L;    // 0x23C Регистр-аккумулятор АС15

}DSP_ALU_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_ALU (*((DSP_ALU_REGS_TYPE*)DSP0_ALU_ADDR))
#define DSP1_ALU (*((DSP_ALU_REGS_TYPE*)DSP1_ALU_ADDR))

//==============================================================================
//         Регистры Устройства генерации адресов памяти данных AGU (DSP_AGU)
//==============================================================================

//================================================
// DSP_AGU структура
//================================================
typedef volatile struct  {
   union DATA_REG_COMMON_UNION      A0;    // Регистр адреса A0
   union DATA_REG_COMMON_UNION      A1;    // Регистр адреса A1
   union DATA_REG_COMMON_UNION      A2;    // Регистр адреса A2
   union DATA_REG_COMMON_UNION      A3;    // Регистр адреса A3
   union DATA_REG_COMMON_UNION      A4;    // Регистр адреса A4
   union DATA_REG_COMMON_UNION      A5;    // Регистр адреса A5
   union DATA_REG_COMMON_UNION      A6;    // Регистр адреса A6
   union DATA_REG_COMMON_UNION      A7;    // Регистр адреса A7
   union DATA_REG_COMMON_UNION      I0;    // Регистр индекса I0
   union DATA_REG_COMMON_UNION      I1;    // Регистр индекса I1
   union DATA_REG_COMMON_UNION      I2;    // Регистр индекса I2
   union DATA_REG_COMMON_UNION      I3;    // Регистр индекса I3
   union DATA_REG_COMMON_UNION      I4;    // Регистр индекса I4
   union DATA_REG_COMMON_UNION      I5;    // Регистр индекса I5
   union DATA_REG_COMMON_UNION      I6;    // Регистр индекса I6
   union DATA_REG_COMMON_UNION      I7;    // Регистр индекса I7
   union DATA_REG_COMMON_UNION      M0;    // Регистр модификатора M0
   union DATA_REG_COMMON_UNION      M1;    // Регистр модификатора M1
   union DATA_REG_COMMON_UNION      M2;    // Регистр модификатора M2
   union DATA_REG_COMMON_UNION      M3;    // Регистр модификатора M3
   union DATA_REG_COMMON_UNION      M4;    // Регистр модификатора M4
   union DATA_REG_COMMON_UNION      M5;    // Регистр модификатора M5
   union DATA_REG_COMMON_UNION      M6;    // Регистр модификатора M6
   union DATA_REG_COMMON_UNION      M7;    // Регистр модификатора M7
   union DATA_REG_COMMON_UNION      AT;    // Регистр адреса AT
   union DATA_REG_COMMON_UNION      IT;    // Регистр индекса IT
   union DATA_REG_COMMON_UNION      MT;    // Регистр модификатора MT
   union DATA_REG_COMMON_UNION      DT;    // Регистр модификатора DT
   union DATA_REG_COMMON_UNION     reserv0;    // 0xF0 Резерв
   union DATA_REG_COMMON_UNION     reserv1;    // 0xF4 Резерв
   union DATA_REG_COMMON_UNION     reserv2;    // 0xF8 Резерв
   union DATA_REG_COMMON_UNION      IVAR;    // 0xFC Регистр адреса вектора прерывания
}DSP_AGU_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_AGU (*((DSP_AGU_REGS_TYPE*)DSP0_AGU_ADDR))
#define DSP1_AGU (*((DSP_AGU_REGS_TYPE*)DSP1_AGU_ADDR))

//==============================================================================
//         Регистры данных RF (DSP_RF)
//==============================================================================
typedef volatile struct  {
    union DATA_REG_COMMON_UNION    R0_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R2_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R4_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R6_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R8_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R10_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R12_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R14_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R16_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R18_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R20_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R22_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R24_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R26_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R28_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R30_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R1_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R3_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R5_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R7_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R9_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R11_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R13_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R15_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R17_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R19_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R21_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R23_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R25_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R27_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R29_L;            // Регистр данных
    union DATA_REG_COMMON_UNION    R31_L;            // Регистр данных
    int reserv64[64];            // Резерв
    struct DSP_LONG_REG    R1_D;    // Регистр данных
    struct DSP_LONG_REG    R3_D;    // Регистр данных
    struct DSP_LONG_REG    R5_D;    // Регистр данных
    struct DSP_LONG_REG    R7_D;    // Регистр данных
    struct DSP_LONG_REG    R9_D;    // Регистр данных
    struct DSP_LONG_REG    R11_D;    // Регистр данных
    struct DSP_LONG_REG    R13_D;    // Регистр данных
    struct DSP_LONG_REG    R15_D;    // Регистр данных
    struct DSP_LONG_REG    R17_D;    // Регистр данных
    struct DSP_LONG_REG    R19_D;    // Регистр данных
    struct DSP_LONG_REG    R21_D;    // Регистр данных
    struct DSP_LONG_REG    R23_D;    // Регистр данных
    struct DSP_LONG_REG    R25_D;    // Регистр данных
    struct DSP_LONG_REG    R27_D;    // Регистр данных
    struct DSP_LONG_REG    R29_D;    // Регистр данных
    struct DSP_LONG_REG    R31_D;    // Регистр данных
}DSP_RF_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_RF (*((DSP_RF_REGS_TYPE*)DSP0_RF_ADDR))
#define DSP1_RF (*((DSP_RF_REGS_TYPE*)DSP1_RF_ADDR))

//==============================================================================
// Регистры-аккумуляторы (DSP_AC)
//==============================================================================

typedef volatile struct  {
    union DATA_REG_COMMON_UNION      AC0_L;    // 0x200 Регистр-аккумулятор АС0
    union DATA_REG_COMMON_UNION      AC1_L;    // Регистр-аккумулятор АС1
    union DATA_REG_COMMON_UNION      AC2_L;    // Регистр-аккумулятор АС2
    union DATA_REG_COMMON_UNION      AC3_L;    // Регистр-аккумулятор АС3
    union DATA_REG_COMMON_UNION      AC4_L;    // Регистр-аккумулятор АС4
    union DATA_REG_COMMON_UNION      AC5_L;    // Регистр-аккумулятор АС5
    union DATA_REG_COMMON_UNION      AC6_L;    // Регистр-аккумулятор АС6
    union DATA_REG_COMMON_UNION      AC7_L;    // Регистр-аккумулятор АС7
    union DATA_REG_COMMON_UNION      AC8_L;    // Регистр-аккумулятор АС8
    union DATA_REG_COMMON_UNION      AC9_L;    // Регистр-аккумулятор АС9
    union DATA_REG_COMMON_UNION      AC10_L;    // Регистр-аккумулятор АС10
    union DATA_REG_COMMON_UNION      AC11_L;    // Регистр-аккумулятор АС11
    union DATA_REG_COMMON_UNION      AC12_L;    // Регистр-аккумулятор АС12
    union DATA_REG_COMMON_UNION      AC13_L;    // Регистр-аккумулятор АС13
    union DATA_REG_COMMON_UNION      AC14_L;    // Регистр-аккумулятор АС14
    union DATA_REG_COMMON_UNION      AC15_L;    // 0x23C Регистр-аккумулятор АС15

}DSP_AC_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_AC (*((DSP_AC_REGS_TYPE*)DSP0_AC_ADDR))
#define DSP1_AC (*((DSP_AC_REGS_TYPE*)DSP1_AC_ADDR))

//==============================================================================
//        Отладочные регистры (DSPn_DB)
//==============================================================================

//================================================
//    dbDCSR
//================================================
struct __attribute__ ((packed,aligned(4))) dbDCSR_BITS { // bits  description
   Uint16  rsvd_1    :2;        // не используется
   Uint16  dbBRK    :1;        // флаг останов исполнения программы в режиме отладки
   Uint16  rsvd_2    :9;        // не используется
   Uint16  dbRUN    :1;        // состояние исполнения программы в режиме отладки
   Uint16  rsvd_3    :1;        // не используется
};
union dbDCSR_REG {
   Uint32                data;
   struct dbDCSR_BITS    bits;
};
//================================================
// DSPn_DB структура
//================================================
typedef volatile struct  {
   union dbDCSR_REG                 dbDCSR;        // Регистр управления в режиме отладки
   union DATA_REG_COMMON_UNION         reserv0_6[5];    // 0x4...0x14 Резерв
   union DATA_REG_COMMON_UNION      Cnt_RUN;    // Счетчик тактов
   union DATA_REG_COMMON_UNION         reserv1;    // 0x0C Резерв
   union DATA_REG_COMMON_UNION      dbPCe;        // Программный счетчик, стадия e
   union DATA_REG_COMMON_UNION      dbPCa;        // Программный счетчик, стадия a
   union DATA_REG_COMMON_UNION      dbPCf;        // Программный счетчик, стадия f
   union DATA_REG_COMMON_UNION      dbPCd;        // Программный счетчик, стадия d

   union DATA_REG_COMMON_UNION      dbPCe1;        // Программный счетчик, стадия e1
   union DATA_REG_COMMON_UNION      dbPCe2;        // Программный счетчик, стадия e2
   union DATA_REG_COMMON_UNION      dbPCe3;        // Программный счетчик, стадия e3
   union DATA_REG_COMMON_UNION      dbSAR;        // Регистр адреса останова 0 в режиме отладки
   union DATA_REG_COMMON_UNION      dbCNTR;        // Счетчик исполненных команд в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR1;        // Регистр адреса останова 1 в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR2;        // Регистр адреса останова 2 в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR3;        // Регистр адреса останова 3 в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR4;        // Регистр адреса останова 4 в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR5;        // Регистр адреса останова 5 в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR6;        // Регистр адреса останова 6 в режиме отладки
   union DATA_REG_COMMON_UNION      dbSAR7;        // Регистр адреса останова 7 в режиме отладки
}DSP_DB_REGS_TYPE;
//================================================
// Макросы
//================================================
#define DSP0_DB (*((DSP_DB_REGS_TYPE*)DSP0_DB_ADDR))
#define DSP1_DB (*((DSP_DB_REGS_TYPE*)DSP1_DB_ADDR))
//==============================================================================

#endif /* NVCOM02_DSP_REGS_H_ */
