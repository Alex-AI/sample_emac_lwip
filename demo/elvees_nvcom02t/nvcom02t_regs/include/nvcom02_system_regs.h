/*******************************************************************************
 * nvcom02_system_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 *
 * Пример использования смотри в << nvcom02_dma_mem_ch_regs.h >>
 *
 ******************************************************************************/
#ifndef NVCOM02_SYSTEM_REGS_H_
#define NVCOM02_SYSTEM_REGS_H_

//==============================================================================
// CR_PLL
//==================================================
struct __attribute__ ((packed,aligned(4))) SYSTEM_CR_PLL_BITS {// bits  description
       Uint16  CLK_SEL_CORE    :7;    // Коэффициент умножения/деления входной частоты PLL_CORE
       Uint16  rsrv_1        :1;    // Резерв
       Uint16  CLK_SEL_MPORT:7;    // Коэффициент умножения/деления входной частоты PLL_MPORT
       Uint16  rsrv_2        :1;    // Резерв
       Uint16  CLK_SEL_DSP    :7;    // Коэффициент умножения/деления входной частоты PLL_DSP
       Uint16  PLL_DSP_EN    :1;    // Выбор источника тактовой частоты для работы DSP
       Uint16  rsrv_3        :8;    // Резерв
};

union SYSTEM_CR_PLL_REG {
   Uint32                        data;
   struct SYSTEM_CR_PLL_BITS    bits;
};

//==================================================
// CLK_EN
//==================================================
struct __attribute__((packed,aligned(4))) SYSTEM_CLK_EN_BITS {         // bits  description
       Uint16  CLKDSBL_CORE_1    :1;    // Отключение тактовой частоты CPU, CRAM, коммутатора AXI, MPORT, UART, I2C, MFBSP0, TIMER
       Uint16  CLKDSBL_CPU        :1;    // Отключение тактовой частоты CPU
       Uint16  CLKDSBL_CORE_2    :1;    // Отключение тактовой частоты CPU, CRAM, коммутатора AXI, MPORT, UART, I2C, MFBSP0,
       Uint16  rsrv_1            :1;    // Резерв
       Uint16  CLKEN_DSP        :2;    // Управление включением тактовой частоты DSP1 и DSP0
       Uint16  rsrv_2            :2;    // Резерв
       Uint16  CLKEN_MFBSP        :1;    // Управление включением тактовой частоты MFBSP1, MFBSP2, MFBSP3, DMA_MFBSP
       Uint16  rsrv_3            :2;    // Резерв
       Uint16  CLKEN_DMA_MEM_CH    :1;    // Управление включением тактовой частоты каналов DMA MEM_CH
       Uint16  rsrv_4            :5;    // Резерв
       Uint16  CLKEN_VPIN        :1;    // Управление включением тактовой частоты  VPIN, DMA_VPIN
       Uint16  CLKEN_VPOUT        :1;    // Управление включением тактовой частоты  VPOUT, DMA_VPOUT
       Uint16  CLKEN_EMAC        :1;    // Управление включением тактовой частоты  EMAC, DMA_EMAC
       Uint16  rsrv_5            :10;// Резерв
       Uint16  CLKEN_MCC        :1;    // Управление включением тактовой частоты MCC, поступающей с контактной площадки
};

union SYSTEM_CLK_EN_REG {
   Uint32                        data;
   struct SYSTEM_CLK_EN_BITS    bits;
};

//==================================================
// CSR
//==================================================
struct  __attribute__((packed,aligned(4)))  SYSTEM_SCSR_BITS {    // bits  description
       Uint16  FM:1;        // Режим преобразования виртуальных адресов CPU в физические адреса
       Uint16  TR_CRAM    :1;    // Режим размещения векторов прерываний при BEV = 0
       Uint16  rsrv_1    :9;    // Резерв
       Uint16  TST_CACHE:1;    // Режим работы кэш программ и кэш данных
       Uint16  FLUSH_I    :1;    // При записи 1 в данный разряд кэш команд CPU останавливается в исходное состояние, то есть ее
                                              // содержимое девалидируется
       Uint16  rsrv_2    :1;    // Резерв
       Uint16  FLUSH_D    :1;    // При записи 1 в данный разряд кэш данных CPU останавливается в исходное состояние, то есть ее
                                              // содержимое девалидируется
       Uint16  rsrv_3    :16;// Резерв
};

union SYSTEM_SCSR_REG {
   Uint32                        data;
   struct SYSTEM_SCSR_BITS        bits;
};

//==================================================
// QSTR0
//==================================================
struct __attribute__ ((packed,aligned(4))) SYSTEM_QSTR0_BITS {     // bits  description
       Uint16  IRQ0            :1;    // Внешние прерывание nIRQ0
       Uint16  IRQ1            :1;    // Внешние прерывание nIRQ1
       Uint16  IRQ2            :1;    // Внешние прерывание nIRQ2
       Uint16  IRQ3            :1;    // Внешние прерывание nIRQ3
       Uint16  UART0        :1;    // Прерывание от UART0
       Uint16  UART1        :1;    // Прерывание от UART1
       Uint16  rsrv_1        :6;    // Резерв
       Uint16  ETH_RX_FRAME    :1;    // Прерывание от контроллера Ethernet по приему кадра или по переполнению входного FIFO
       Uint16  ETH_TX_FRAME    :1;    // Прерывание от контроллера Ethernet по завершению попытки передачи пакета
       Uint16  ETH_DMA_RX    :1;    // Прерывание от DMA контроллера Ethernet по завершению приема
       Uint16  ETH_DMA_TX    :1;    // Прерывание от DMA контроллера Ethernet по завершению передачи
       Uint16  VPIN            :1;    // Прерывание от контроллера VPIN
       Uint16  VPIN_RX        :1;    // Прерывание от канала DMA VPIN по приему массива данных
       Uint16  VPOUT        :1;    // Прерывание от контроллера VPOUT
       Uint16  VPOUT_TX        :1;    // Прерывание от канала DMA VPOUT по передаче массива данных
       Uint16  WDT            :1;    // Прерывание от таймера WDT
       Uint16  IRT1            :1;    // Прерывание от таймера IT1
       Uint16  IRT0            :1;    // Прерывание от таймера IT0
       Uint16  INT_I2C        :1;    // Прерывание от I2C
       Uint16  rsrv_2        :7;    // Резерв
       Uint16  NT_MCC        :1;    // Прерывание от МСС
};

union SYSTEM_QSTR0_REG {
   Uint32                    data;
   struct SYSTEM_QSTR0_BITS    bits;
};

//==================================================
// QSTR1
//==================================================
struct __attribute__ ((packed,aligned(4))) SYSTEM_QSTR1_BITS { // bits  description
       Uint16  MEM_CH0    :1;    // Прерывание от каналов DMA MEM_CH0
       Uint16  MEM_CH1    :1;    // Прерывание от каналов DMA MEM_CH1
       Uint16  MEM_CH2    :1;    // Прерывание от каналов DMA MEM_CH2
       Uint16  MEM_CH3    :1;    // Прерывание от каналов DMA MEM_CH3
       Uint32  rsrv_1    :28;// Резерв
};

union SYSTEM_QSTR1_REG {
   Uint32                    data;
   struct SYSTEM_QSTR1_BITS    bits;
};

//==================================================
// QSTR2
//==================================================
struct __attribute__ ((packed,aligned(4))) SYSTEM_QSTR2_BITS {     // bits  description
       Uint16  SRQ0:1;            // Запрос обслуживания от порта MFBSP0
       Uint16  MFBSP_TXBUF0    :1;    // Формируется, если порт включен на передачу данных
       Uint16  MFBSP_RXBUF0    :1;    // Формируется, если порт включен на прием данных
       Uint16  rsrv_1        :1;    // Резерв
       Uint16  DMA_MFBSP_TX0:1;    // Прерывание от канала DMA порта MFBSP0 при передаче данных
       Uint16  DMA_MFBSP_RX0:1;    // Прерывание от канала DMA порта MFBSP0 при приеме данных
       Uint16  rsrv_2        :2;    // Резерв
       Uint16  SRQ1            :1;    // Запрос обслуживания от порта MFBSP1
       Uint16  MFBSP_TXBUF1    :1;
       Uint16  MFBSP_RXBUF1    :1;
       Uint16  rsrv_3        :1;    // Резерв
       Uint16  DMA_MFBSP_TX1:1;
       Uint16  DMA_MFBSP_RX1:1;
       Uint16  rsrv_4        :2;    // Резерв
       Uint16  SRQ2            :1;    // Запрос обслуживания от порта MFBSP2
       Uint16  MFBSP_TXBUF2    :1;
       Uint16  MFBSP_RXBUF2    :1;
       Uint16  rsrv_5        :1;    // Резерв
       Uint16  DMA_MFBSP_TX2:1;
       Uint16  DMA_MFBSP_RX2:1;
       Uint16  rsrv_6        :2;    // Резерв
       Uint16  SRQ3            :1;    // Запрос обслуживания от порта MFBSP3
       Uint16  MFBSP_TXBUF3    :1;
       Uint16  MFBSP_RXBUF3    :1;
       Uint16  rsrv_7        :1;    // Резерв
       Uint16  DMA_MFBSP_TX3:1;
       Uint16  DMA_MFBSP_RX3:1;
       Uint16  rsrv_8        :2;    // Резерв
};

union SYSTEM_QSTR2_REG {
   Uint32                    data;
   struct SYSTEM_QSTR2_BITS    bits;
};

//==================================================
// IRQM
//==================================================
struct __attribute__ ((packed,aligned(4))) SYSTEM_IRQM_BITS {     // bits  description
       Uint16  IRQ_NULL    :4;    // Обнуление запомненных прерываний при IRQ_MODE = 1
       Uint16  rsrv_1    :4;    // Резерв
       Uint16  IRQ_MODE    :4;    // Режим приема внешних прерываний  nIRQ[3:0]
       Uint32  rsrv_2    :20;// Резерв
};

union SYSTEM_IRQM_REG {
   Uint32                    data;
   struct SYSTEM_IRQM_BITS    bits;
};

//==============================================================================
// SYSTEM структура
//==============================================================================
typedef volatile struct {
   union SYSTEM_CR_PLL_REG      CR_PLL;        // Регистр управления PLL
   union SYSTEM_CLK_EN_REG      CLK_EN;        // Регистр управления отключением частоты от устройств
   union SYSTEM_SCSR_REG          CSR;        // Регистр управления и состояния
   union DATA_REG_COMMON_UNION     reserv3;    // 0x400C
   union DATA_REG_COMMON_UNION  MASKR0;        // Регистр маски прерываний из регистра QST0
   union SYSTEM_QSTR0_REG          QSTR0;        // Регистр прерываний 0
   union DATA_REG_COMMON_UNION  MASKR1;        // Регистр маски прерываний из регистра QST1
   union SYSTEM_QSTR1_REG          QSTR1;        // Регистр прерываний 1
   union DATA_REG_COMMON_UNION  MASKR2;        // Регистр маски прерываний из регистра QST2
   union SYSTEM_QSTR2_REG          QSTR2;        // Регистр прерываний 2
   union DATA_REG_COMMON_UNION     reserv1;    // 0x4028
   union DATA_REG_COMMON_UNION     reserv2;    // 0x402c
   union SYSTEM_IRQM_REG          IRQM;        // Регистр управления режимом приема внешних прерываний nIRQ[3:0]
} SYSTEM_REGS_TYPE;

//==================================================
//    Макрос
//==================================================
#define SYS_REG (*((SYSTEM_REGS_TYPE*)SYS_REG_ADDR))
//==============================================================================


#endif /* NVCOM02_SYSTEM_REGS_H_ */
