/*******************************************************************************
 * nvcom02_mfbsp_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 *    Содержит макросы для доступа к регистрам MFBSPn, MFBSP(n) где (n=0...3)
 *
 *  Пример использования смотри в < nvcom02_dma_mem_ch_regs.h >
 *
 ******************************************************************************/
#ifndef NVCOM02_MFBSP_REGS_H_
#define NVCOM02_MFBSP_REGS_H_

//==============================================================================
// CSR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_MCSR_BITS {         // bits  description
    Uint16  LEN                :1;        // 0  В режиме I2S/SPI должен быть установлен в 0
    Uint16  LTRAN            :1;        // 1  Режим работы порта
    Uint16  LCLK_RATE_0        :1;        // 2  Делитель частоты LPORT
    Uint16  LSTAT            :2;        // 4:3 состояние буфера
    Uint16  LRERR            :1;        // 5  Ошибка приема данных
    Uint16  LDW                :1;        // 6  Разрядность внешней шины данных
    Uint16  SRQ_TX            :1;        // 7  Признак запроса обслуживания на передачу данных
    Uint16  SRQ_RX            :1;        // 8  Признак запроса обслуживания на прием  данных
    Uint16  SPI_I2S_EN        :1;        // 9  Включение режима SPI/I2S
    Uint16  rsrv_1            :1;        // 10 Резерв
    Uint16  LCLK_RATE_1_4    :4;        // 14:11 Делитель частоты LPORT
    Uint32  rsrv_2            :17;    // 31:15 Резерв
};

union MFBSP_MCSR_REG {
   Uint32                    data;
   struct MFBSP_MCSR_BITS    bits;
};

//==================================================
// DIR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_DIR_BITS {         // bits  description
    Uint16  RCLK_DIR        :1;    // Направление вывода RCLK
    Uint16  TCLK_DIR        :1;    // Направление вывода TCLK
    Uint16  RCS_DIR            :1;    // Направление вывода RWS
    Uint16  TCS_DIR            :1;    // Направление вывода TWS
    Uint16  RD_DIR            :1;    // Направление вывода RD
    Uint16  TD_DIR            :1;    // Направление вывода TD
    Uint16  LDAT_DIR        :4;    // Направление выводов LDAT[7:4]
};

union MFBSP_DIR_REG {
   Uint32                    data;
   struct MFBSP_DIR_BITS    bits;
};

//==================================================
// GPIO_DR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_GPIO_DR_BITS {     // bits  description
    Uint16  LACK            :1;            //
    Uint16  LCLK            :1;            //
    Uint16  LDAT            :8;            //
};

union MFBSP_GPIO_DR_REG {
   Uint32                        data;
   struct MFBSP_GPIO_DR_BITS    bits;
};

//==================================================
// TCTR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_TCTR_BITS {     // bits  description
    Uint16  TEN                :1;    // Разрешение работы передатчика
    Uint16  TMODE            :1;    // Режим работы передатчика
    Uint16  TD_ZER_EN        :1;    // Обнуление избыточных бит передаваемого слова
    Uint16  SS_DO            :1;    // управление выводами SS
    Uint16  rsrv_1            :5;    // Резерв
    Uint16  TDSPMODE        :1;    // Формат передачи данных
    Uint16  TNEG            :1;    // Полярность тактового сигнала передатчика
    Uint16  TDEL            :1;    // Задержка начала передачи данных на такт
    Uint16  TWORDCNT        :6;    // Число слов во фрейме
    Uint16  TCSNEG            :1;    // Полярность управляющего сигнала передатчика
    Uint16  TMBF            :1;    // Порядок передачи бит
    Uint16  TWORDLEN        :5;    // Длина передаваемого слова
    Uint16  TPACK            :1;    // Включение режима распаковки
    Uint16  rsrv_2            :1;    // Резерв
    Uint16  TSWAP            :1;    // Порядок распаковки 32-х разрядного слова
    Uint16  TCLK_CONT        :1;    // Включение непрерывного формирования сигнала TCLK
    Uint16  TCS_CONT        :1;    // Включение непрерывного формирования сигнала TWS
    Uint16  SS_0            :1;    // биты управления шиной Slave Select
    Uint16  SS_1            :1;    //
};

union MFBSP_TCTR_REG {
   Uint32                    data;
   struct MFBSP_TCTR_BITS    bits;
};

//==================================================
// RCTR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_RCTR_BITS {         // bits  description
    Uint16  REN                :1;    // Разрешение работы приёмника
    Uint16  RMODE            :1;    // Режим работы приёмника
    Uint16  RCLK_CP            :1;    // Дублирование TCLK
    Uint16  RCS_CP            :1;    // Дублирование сигнала TWS
    Uint16  rsrv_1            :5;    // Резерв
    Uint16  RDSPMODE        :1;    // Формат передачи данных
    Uint16  RNEG            :1;    // Полярность тактового сигнала приёмника
    Uint16  RDEL            :1;    // Задержка начала приёма данных на такт
    Uint16  RWORDCNT        :6;    // Число слов во фрейме
    Uint16  RCSNEG            :1;    // Полярность управляющего сигнала приёмника
    Uint16  RMBF            :1;    // Порядок передачи бит
    Uint16  RWORDLEN        :5;    // Длина принимаемого слова
    Uint16  RPACK            :1;    // Включение режима распаковки
    Uint16  RSIGN            :1;    // Значение заполнителя
    Uint16  RSWAP            :1;    // Порядок распаковки 32-х разрядного слова
    Uint16  RCLK_CONT        :1;    // Включение непрерывного формирования сигнала TCLK
    Uint16  RCS_CONT        :1;    // Включение непрерывного формирования сигнала TWS
    Uint16  rsrv_2            :2;    // Резерв
};

union MFBSP_RCTR_REG {
   Uint32                    data;
   struct MFBSP_RCTR_BITS    bits;
};

//==================================================
// TSR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_TSR_BITS {         // bits  description
    Uint16  TBE                :1;    // Буфер передачи пуст
    Uint16  TBF                :1;    // Буфер передачи полон
    Uint16  TBHF            :1;    // Буфер передачи заполнен на половину или более
    Uint16  TBLL            :1;    // Достигнут порог прерывания в буфере передачи
    Uint16  TSBE            :1;    // Буфер пересинхронизации в направлении передачи пуст
    Uint16  TSBF            :1;    // Буфер пересинхронизации в направлении передачи полон
    Uint16  TERR            :1;    // Ошибка передачи
    Uint16  TRUN            :1;    // Идёт передача
    Uint16  TXBUF_R            :1;    // Прерывание MFBSP_TXBUF c механизмом автоматического сброса при чтении TSR
    Uint16  TXBUF_D            :1;    // Прерывание MFBSP_TXBUF без механизма автоматического сброса при чтении TSR
    Uint16  TXBUF            :1;    // Результирующее прерывание MFBSP_TXBUF
    Uint16  rsrv_1            :5;    // Резерв
    Uint16  TLEV            :3;    // Порог прерывания от буфера передачи
    Uint16  rsrv_2            :1;    // Резерв
    Uint16  TBES            :3;    // Эффективный размер буфера передачи
    Uint16  rsrv_3            :1;    // Резерв
    Uint16  TB_DIFF            :4;    // Количество свободных 64-разрядных позиций в буфере передачи
    Uint16  rsrv_4            :4;    // Резерв
};

union MFBSP_TSR_REG {
   Uint32                        data;
   struct MFBSP_TSR_BITS        bits;
};

//==================================================
// RSR
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_RSR_BITS {         // bits  description
    Uint16  RBE                :1;    // Буфер приёма пуст
    Uint16  RBF                :1;    // Буфер приёма полон
    Uint16  RBHF            :1;    // Буфер приёма заполнен на половину или более
    Uint16  RBLL            :1;    // Достигнут порог прерывания в буфере приёма
    Uint16  RSBE            :1;    // Буфер пересинхронизации в направлении приёма пуст
    Uint16  RSBF            :1;    // Буфер пересинхронизации в направлении приёма полон
    Uint16  RERR            :1;    // Ошибка передачи
    Uint16  RRUN            :1;    // Идёт приём
    Uint16  RXBUF_R            :1;    // Прерывание MFBSP_TXBUF c механизмом автоматического сброса при чтении RSR
    Uint16  RXBUF_D            :1;    // Прерывание MFBSP_TXBUF без механизма автоматического сброса при чтении RSR
    Uint16  RXBUF            :1;    // Результирующее прерывание MFBSP_TXBUF
    Uint16  rsrv_1            :5;    // Резерв
    Uint16  RLEV            :3;    // Порог прерывания от буфера приёма
    Uint16  rsrv_2            :5;    // Резерв
    Uint16  RB_DIFF            :4;    // Количество свободных 64-разрядных позиций в буфере приёма (мах 8)
    Uint16  rsrv_3            :4;    // Резерв
};

union MFBSP_RSR_REG {
   Uint32                    data;
   struct MFBSP_RSR_BITS    bits;
};

//==================================================
// TCTR_RATE
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_TCTR_RATE_BITS { // bits  description
    Uint16  TCLK_RATE        :10;    // Делитель частоты передатчика
    Uint16  rsrv_1            :2;        // Резерв
    Uint16  TSS_RATE        :4;        // Если сигнал SS формируется передатчиком, то задает время удержания сигнала SS в высоком уровне между
                                                   // передачами слов
    Uint16  TCS_RATE        :16;    // Делитель частоты управляющего сигнала передатчика
};

union MFBSP_TCTR_RATE_REG {
   Uint32                        data;
   struct MFBSP_TCTR_RATE_BITS    bits;
};

//==================================================
// RCTR_RATE
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_RCTR_RATE_BITS { // bits  description
    Uint16  RCLK_RATE        :10;    // Делитель частоты приёмника
    Uint16  rsrv_1            :2;        // Резерв
    Uint16  RSS_RATE        :4;        // Если сигнал SS формируется приёмником, то задает время удержания сигнала SS в высоком уровне между
                                                   // передачами слов.
       Uint16  RCS_RATE        :16;    // Делитель частоты управляющего сигнала приёмника
};

union MFBSP_RCTR_RATE_REG {
   Uint32                        data;
   struct MFBSP_RCTR_RATE_BITS    bits;
};

//==================================================
// TSTART
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_TSTART_BITS {     // bits  description
    Uint16  TEN                :1;        // Разрешение работы передатчика
    Uint32  rsrv_1            :31;    // Резерв
};

union MFBSP_TSTART_REG {
   Uint32                    data;
   struct MFBSP_TSTART_BITS    bits;
};

//==================================================
// RSTART
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_RSTART_BITS {     // bits  description
    Uint16  REN                :1;        // Разрешение работы приемника
    Uint32  rsrv_1            :31;    // Резерв
};

union MFBSP_RSTART_REG {
   Uint32                    data;
   struct MFBSP_RSTART_BITS    bits;
};

//==================================================
// EMERG_MFBSP
//==================================================
struct __attribute__((packed,aligned(4))) MFBSP_EMERG_MFBSP_BITS {     // bits  description
    Uint16  RST_LPTBUF        :1;    // Сброс буфера линкового порта и буфера пересинхронизации направления приёма.
    Uint16  RST_TXBUF        :1;    // Сброс буфера передачи последовательного порта и буфера пересинхронизации направления передачи
    Uint16  RST_RXBUF        :1;    // Сброс буфера приема последовательного порта и буфера пересинхронизации направления приема.
    Uint16  rsrv_1            :1;    // Резерв
    Uint16  TX_DBG            :1;    // Включение аварийной прокачки данных канала DMA направления передачи
    Uint16  RX_DBG            :1;    // Включение аварийной прокачки данных канала DMA направления приема.
    Uint32  rsrv_2            :26;    // Резерв
};

union MFBSP_EMERG_MFBSP_REG {
   Uint32                            data;
   struct MFBSP_EMERG_MFBSP_BITS    bits;
};

//==================================================
// IMASK_MFBSP
//==================================================
struct  __attribute__((packed,aligned(4)))  MFBSP_IMASK_MFBSP_BITS {     // bits  description
    Uint16  LPT_IRQ_EN         :1;        // Разрешение прерывания по запросу на обслуживание
    Uint16  rsrv_1             :3;        // Резерв
    Uint16  TX_ERR_IRQ_EN     :1;        // Разрешение прерывания при переполнении буфера приема
    Uint16  TX_LEV_IRQ_EN     :1;        // Разрешение прерывания по уровню заполнения буфера передачи
    Uint16  TXBUF_R_EN         :1;        // Разрешение автоматического сброса прерывания MFBSP_TXBUF
    Uint16  rsrv_2             :5;        // Резерв
    Uint16  RX_ERR_IR_EN    :1;        // Разрешение прерывания при переполнении буфера приема
    Uint16  RX_LEV_IRQ_EN     :1;        // Разрешение прерывания по уровню заполнения буфера  приема
    Uint16  RXBUF_R_EN         :1;        // Разрешение автоматического сброса прерывания MFBSP_RXBUF
    Uint16  rsrv_3             :16;    // Резерв
};

union MFBSP_IMASK_MFBSP_REG {
   Uint32                            data;
   struct MFBSP_IMASK_MFBSP_BITS    bits;
};

//==============================================================================
// MFBSP структура
//==============================================================================
typedef volatile struct  {
   union {
     union  DATA_REG_COMMON_UNION    TX;        // Буфер передачи данных
     union  DATA_REG_COMMON_UNION    RX;        // Буфер приема данных
   };
   union MFBSP_MCSR_REG          CSR;        // Регистр управления и состояния
   union MFBSP_DIR_REG          DIR;        // Регистр управления направлением выводов порта ввода вывода
   union MFBSP_GPIO_DR_REG         GPIO_DR;    // Регистр данных порта ввода-вывода
   union MFBSP_TCTR_REG          TCTR;        // Регистр управления передатчиком
   union MFBSP_RCTR_REG          RCTR;        // Регистр управления приёмником
   union MFBSP_TSR_REG          TSR;        // Регистр состояния передатчика
   union MFBSP_RSR_REG          RSR;        // Регистр состояния приёмника
   union MFBSP_TCTR_RATE_REG       TCTR_RATE;    // Регистр управления темпом передачи данных
   union MFBSP_RCTR_RATE_REG       RCTR_RATE;    // Регистр управления темпом приёма данных
   union MFBSP_TSTART_REG          TSTART;        // псевдорегистр ten – запуск/останов передатчика без изменения настроек передатчика
   union MFBSP_RSTART_REG          RSTART;        // псевдорегистр ren – запуск/останов приемника без изменения настроек приемника
   union MFBSP_EMERG_MFBSP_REG     EMERG_MFBSP;// Регистр аварийного управления портом
   union MFBSP_IMASK_MFBSP_REG     IMASK_MFBSP;// Регистр маски прерываний от порта

}MFBSP_REGS_TYPE;

//==================================================
//    Макросы
//==================================================
#define MFBSP0 (*((MFBSP_REGS_TYPE*)MFBSP0_ADDR))
#define MFBSP1 (*((MFBSP_REGS_TYPE*)MFBSP1_ADDR))
#define MFBSP2 (*((MFBSP_REGS_TYPE*)MFBSP2_ADDR))
#define MFBSP3 (*((MFBSP_REGS_TYPE*)MFBSP3_ADDR))
//=================================================================
#define MFBSP(num) (*((MFBSP_REGS_TYPE*)(MFBSP0_ADDR+0x100*num)))
//==============================================================================



#endif /* NVCOM02_MFBSP_REGS_H_ */
