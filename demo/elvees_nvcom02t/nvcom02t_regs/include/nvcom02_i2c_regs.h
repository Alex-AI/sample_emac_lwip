/*******************************************************************************
 * nvcom02_i2c_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 *
 * Пример использования смотри в << nvcom02_dma_mem_ch_regs.h >>
 *
 ******************************************************************************/
#ifndef NVCOM02_I2C_REGS_H_
#define NVCOM02_I2C_REGS_H_

//==============================================================================
// CTR
//==================================================
struct __attribute__ ((packed,aligned(4))) I2C_CTR_BITS {         // bits  description
       Uint16  rsrv_1    :6;    // Резерв
       Uint16  IEN        :1;    // Разрешение прерывания от порта I2C
       Uint16  EN        :1;    // Разрешение работы порта I2C
       Uint16  PRST        :1;    // Программный сброс
       Uint16  TM_CNT    :1;    // Разрешение режима тестирования счетчика предделителя частоты
       Uint16  TICK        :1;    // Бит тестирования регистра счетчика PR_СNT
};

union I2C_CTR_REG {
   Uint32                data;
   struct I2C_CTR_BITS    bits;
};

//==================================================
// TXR
//==================================================
struct __attribute__ ((packed,aligned(4))) I2C_TXR_BITS {         // bits  description
        Uint16  RW    :1; // При передаче байта данных этот бит задает младший разряд передаваемых данных;
                        // при передаче адреса ведомого устройства этот бит задает направление    передачи данных
        Uint16  TXD    :7; // Передаваемые данные
};

union I2C_TXR_REG {
   Uint32                data;
   struct I2C_TXR_BITS    bits;
};

//==================================================
// CR
//==================================================
struct __attribute__ ((packed,aligned(4))) I2C_CR_BITS {         // bits  description
       Uint16  IACK        :1;        // Подтверждение прерывания
       Uint16  rsrv_1    :2;        // Резерв
       Uint16  ACK        :1;        // При приеме байта данных от ведомого устройства задает, что выставит I2C порт
       Uint16  SND        :1;        // Пересылка байта данных в ведомое устройство и прием бита ACK/NACK от ведомого устройства
       Uint16  RCV        :1;        // Прием байта данных от ведомого устройства и пересылка бита ACK/NACK в ведомое устройство
       Uint16  STO        :1;        // Генерация состояния STOP на линии
       Uint16  STA        :1;        // Генерация состояния START (repeated START) на линии
};

union I2C_CR_REG {
   Uint32                data;
   struct I2C_CR_BITS    bits;
};

//==================================================
// SR
//==================================================
struct __attribute__ ((packed,aligned(4))) I2C_SR_BITS {         // bits  description
       Uint16  IF        :1;        // Признак наличия прерывания
       Uint16  TIP        :1;        // Признак выполнения передачи данных портом I2C
       Uint16  rsrv_1    :3;        // Резерв
       Uint16  AL        :1;        // Признак того, что порт I2C проиграл арбитраж
       Uint16  BUSY        :1;        // Признак того, что I2C интерфейс занят, то есть выполняется передача  данных.
       Uint16  RXACK    :1;        // Принятый бит ACK/NACK от ведомого устройства после пересылки байта данных
};

union I2C_SR_REG {
   Uint32                data;
   struct I2C_SR_BITS    bits;
};

//==================================================
// SYNC
//==================================================
struct __attribute__((packed,aligned(4))) I2C_SYNC_BITS {     // bits  description
    Uint16  WAIT_PER    :16;    // Количество тактов системной частоты при высоком уровне сигнала SLC, после которых производить синхронизацию.
    Uint16  SYNC_EN        :1;       // Включение возможности синхронизации
};

union I2C_SYNC_REG {
   Uint32                data;
   struct I2C_SYNC_BITS    bits;
};

//==============================================================================
// I2C структура
//==============================================================================
typedef volatile struct  {
    union DATA_REG_COMMON_UNION    PRER;    // Регистр предделителя частоты
    union I2C_CTR_REG              CTR;    // Регистр управления
    union I2C_TXR_REG              TXR;    // Регистр передачи данных
    union DATA_REG_COMMON_UNION    RXR;    // Регистр приема данных
    union I2C_CR_REG              CR;        // Регистр команд
    union I2C_SR_REG              SR;        // Регистр состояния
    union DATA_REG_COMMON_UNION    PR_CNT;    // Счетчик предделителя частоты
    union I2C_SYNC_REG            SYNC;    // Регистр синхронизации
}I2C_REGS_TYPE;

//==================================================
//    Макрос
//==================================================
#define I2C (*((I2C_REGS_TYPE*)I2C_ADDR))
//==============================================================================


#endif /* NVCOM02_I2C_REGS_H_ */
