/*******************************************************************************
 * nvcom02_mport_regs.h
 *
 *  Created on: 02.04.2014
 *       
 *******************************************************************************
 *
 * Пример использования смотри в << nvcom02_dma_mem_ch_regs.h >>
 *
 ******************************************************************************/
#ifndef NVCOM02_MPORT_REGS_H_
#define NVCOM02_MPORT_REGS_H_

//==============================================================================
// CSCON
//==================================================
struct __attribute__ ((packed,aligned(4))) EXTMEM_CSCON_BITS {// bits  description
       Uint16  CSMASK    :8;    // Разряды маски 31:24 при определении базового адреса блока памяти.
       Uint16  CSBA        :8;    // Разряды 31:24 базового адреса блока памяти.
       Uint16  WS        :4;    // Число тактов ожидания при обращении к блоку памяти.
       Uint16  E        :1;    // Разрешение формирования сигнала nCS[0].
       Uint16  T        :2;    // Тип памяти блока памяти.
       Uint16  rsrv_1    :1;    // Резерв
       Uint16  rsrv_2    :8;    // Резерв
};

union EXTMEM_CSCON_REG {
   Uint32                    data;
   struct EXTMEM_CSCON_BITS    bits;
};

//==================================================
// CSCON4
//==================================================
struct __attribute__((packed,aligned(4))) EXTMEM_CSCON4_BITS { // bits  description
       Uint16  rsvd_1    :16;    // Резерв
       Uint16  WS        :4;         // Число тактов ожидания при обращении к блоку памяти.
       Uint16  rsrv_2    :12;    // Резерв
};

union EXTMEM_CSCON4_REG {
   Uint32                        data;
   struct EXTMEM_CSCON4_BITS    bits;
};

//==================================================
// SDRCON
//==================================================
struct  EXTMEM_SDRCON_BITS {// bits  description
       Uint16  PS        :3;    // Размер страницы микросхем SDRAM, подключенных к MPORT
       Uint16  rsrv_1    :1;    // Резерв
       Uint16  CL        :3;    // Задержка данных при чтении (CAS latency)
       Uint16  MOBILE    :1;    // Тип памяти, подключенной к MPORT
       Uint16  DS        :2;    // Мощность выходов микросхем Mobile SDRAM, подключенных к контроллеру (Drive Strength)
       Uint16  TCSR        :2;    // Режим управления температурным датчиком Mobile  SDRAM
       Uint16  rsrv_2    :1;    // Резерв
       Uint16  PASR        :3;    // Режим саморегенерации Mobile SDRAM (Partial-Array Self Refresh)
       Uint16  TRFR        :14;// Период регенерации SDRAM в тактах частоты SCLK
       Uint16  rsrv_3    :2; // Резерв
};

union EXTMEM_SDRCON_REG {
   Uint32                        data;
   struct EXTMEM_SDRCON_BITS    bits;
};

//==================================================
// SDRTMR
//==================================================
struct __attribute__ ((packed,aligned(4))) EXTMEM_SDRTMR_BITS {// bits  description
       Uint16  TWR        :2;    // Минимальная задержка между записью данных и командой Precharge(Write recovery).
       Uint16  rsrv_1    :2;    // Резерв
       Uint16  TRP        :2;    // Минимальный период команд Precharge
       Uint16  rsrv        :2;    // Резерв
       Uint16  TRCD        :2;    // Минимальная задержка между командами Active и  Read/Write
       Uint16  rsrv_2    :6;    // Резерв
       Uint16  TRAS        :4;    // Минимальная задержка между командами Active и Precharge.
       Uint16  TRFC        :4;    // Минимальный интервал между командами Refresh
       Uint16  rsrv_3    :8; // Резерв
};

union EXTMEM_SDRTMR_REG {
   Uint32                        data;
   struct EXTMEM_SDRTMR_BITS    bits;
};

//==================================================
// SDRCSR
//==================================================
struct __attribute__ ((packed,aligned(4))) EXTMEM_SDRCSR_BITS {         // bits  description
       Uint16  INIT        :1;        // выполняет инициализацию SDRAM с параметрами
       Uint16  AREF        :1;        // режим авторегенерации SDRAM / признак окончания команды.
       Uint16  SREF        :1;        // режим саморегенерации / признак окончания команды.
       Uint16  PWDN        :1;        // режим пониженного потребления / признак окончания команды.
       Uint16  EXIT        :1;        // вывод SDRAM из режимов саморегенерации и пониженного потребления / признак окончания команды.
       Uint16  DPD        :1;        // режим пониженного потребления / признак окончания команды.
       Uint32  rsrv_1    :26;    // Резерв
};

union EXTMEM_SDRCSR_REG {
   Uint32                        data;
   struct EXTMEM_SDRCSR_BITS    bits;
};

//==============================================================================
// MPORT структура
//==============================================================================
typedef volatile struct  {
   union EXTMEM_CSCON_REG      CSCON0;    // Регистр конфигурации блока внешней памяти, подключаемого к выводу nCS[0]
   union EXTMEM_CSCON_REG      CSCON1;    // Регистр конфигурации блока внешней памяти, подключаемого к выводу nCS[1]
   union EXTMEM_CSCON_REG      CSCON2;    // Регистр конфигурации блока внешней памяти, подключаемого к выводу nCS[2]
   union EXTMEM_CSCON_REG      CSCON3;    // Регистр конфигурации блока внешней памяти, подключаемого к выводу nCS[3]
   union EXTMEM_CSCON4_REG  CSCON4;    // Регистр конфигурации внешней памяти, не вошедшей
                                       // в блоки памяти, определяемые регистрами CSCON3 - CSCON0
   union EXTMEM_SDRCON_REG  SDRCON;    // Регистр конфигурации SDRAM.
   union EXTMEM_SDRTMR_REG  SDRTMR;    // Регистр параметров SDRAM
   union EXTMEM_SDRCSR_REG  SDRCSR;    // Регистр управления и состояния SDRAM
}EXTMEM_MPORT_REGS_TYPE;

//==================================================
//    Макрос
//==================================================
#define MPORT (*((EXTMEM_MPORT_REGS_TYPE*)MPORT_ADDR))
//==============================================================================

#endif /* NVCOM02_MPORT_REGS_H_ */
