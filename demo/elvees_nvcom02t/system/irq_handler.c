/**
 * @file:
 * This file implements interrupt handling from QSTR0 for 1892VM10YA   
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "irq_handler.h"
#include "nvcom02t.h"

static struct irq_handlers_table_t irq_handlers_table;
static void (**handlers_table_ptr)( ) = &irq_handlers_table.irq_handler_irq0;


/**
 * Interrupt requests handler function
 * Called by an Assembly handler that must be placed at the address of the interrupt vector
 */
void irq_handler()
{

    unsigned int ActiveIRQ = SYS_REG.MASKR0.data & SYS_REG.QSTR0.data;

    enum ID_INTR intr_id;
    for( intr_id = 0; intr_id < IQR_MAX_INTRS; ++intr_id)
    {    

        if(intr_id == ID_INTR_RSRV00)
            intr_id += ID_INTR_RSRV0_SIZE;

        if(intr_id == ID_INTR_RSRV10)
            intr_id += ID_INTR_RSRV1_SIZE;

        if((1 << intr_id)&ActiveIRQ && handlers_table_ptr[intr_id])
            handlers_table_ptr[intr_id]();
    }

}


void irq_set_handler(enum ID_INTR intr_id, void (*handler)())
{
    SYS_REG.MASKR0.data |= (1 << intr_id);
    
    handlers_table_ptr[intr_id] = handler;
}


