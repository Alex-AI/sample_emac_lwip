/**
 * @file:
 * This file implements some functions for working with CPU of 1892VM10YA
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "cpu.h"
#include "nvcom02t.h"
#include "pll.h"


unsigned int virt_to_phys (void* virtaddr)
{
    unsigned int v = (unsigned int)  virtaddr;
    switch (v >> 28 & 0xE)
    {
    default:  return v + 0x40000000;        /* kuseg */
    case 0x8: return v - 0x80000000;        /* kseg0 */
    case 0xA: return v - 0xA0000000;        /* kseg1 */
    case 0xC: return v;                        /* kseg2 */
    case 0xE: return v;                        /* kseg3 */
    }
}


unsigned int cpu_get_freq()
{
    return (XTI_FREQ/2)*SYS_REG.CR_PLL.bits.CLK_SEL_CORE;
}


void cpu_enable_interrupts(enum IRQ_SOURCE irq_sources, int in_sdram)
{

    mips_write_c0_register(C0_CAUSE, 0);

    #define C0_STATUS_ENABLE_FPU       (1 << 29)
    #define C0_STATUS_INTERRUPT_ENABLE  1
    mips_write_c0_register(C0_STATUS, ( (C0_STATUS_ENABLE_FPU)  | (irq_sources) | (C0_STATUS_INTERRUPT_ENABLE)));

    SYS_REG.CSR.bits.TR_CRAM = !in_sdram;

}



void cpu_disable_interrupts(enum IRQ_SOURCE irq_sources)
{
    mips_write_c0_register(C0_CAUSE, 0);
    unsigned int c0_status = 0;
    c0_status = mips_read_c0_register(C0_STATUS);
    c0_status &= ~(irq_sources);
    mips_write_c0_register(C0_STATUS, c0_status);
}

void cpu_lock_interrupts()
{
    unsigned int c0_status = mips_read_c0_register(C0_STATUS);
    c0_status &= ~(1);
    mips_write_c0_register(C0_STATUS, c0_status);
}


void cpu_unlock_interrupts()
{
    unsigned int c0_status = mips_read_c0_register(C0_STATUS);
    c0_status |= (1);
    mips_write_c0_register(C0_STATUS, c0_status);
}



