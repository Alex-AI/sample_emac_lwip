/**
 * @file:
 * This file implements working with interval timers for program delays and system time on 1892VM10YA chip
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "timer.h"
#include "pll.h"


void timer_IT0_sys_time_start()
{
    /* Disable IT0 interrupt */
    IT0.ITCSR.bits.INT = 0;
    /* Tact from XTI = 10 000 000 Hz */
    IT0.ITCSR.bits.CLK_SEL = 1;
    /* Set the prescaler to obtain the 100 000 Hz 
     * prescaler = 100 - 1
     */
    IT0.ITSCALE.data = 99;

    /* Setting the waiting time as maximum */
    IT0.ITPERIOD.data =  0xFFFFFFFF;
    /* Starting timer */
    IT0.ITCSR.bits.EN = 1;
}

static unsigned int sys_now_msec = 0;
unsigned int timer_IT0_get_sys_now()
{
    /* Getting the elapsed time and add to the "sys_now_msec" counter 
     * dividing by 100 we get the elapsed time in milliseconds
     */
    sys_now_msec += (0xFFFFFFFF - IT0.ITCOUNT.data)/100;

    /* Setting the waiting time as maximum */
    IT0.ITCOUNT.data = 0xFFFFFFFF;
    return sys_now_msec;
}




void timer_IT1_intr(unsigned int msec)
{
    IT1.ITSCALE.data = 0;
    IT1.ITCSR.bits.INT = 0;

    IT1.ITCSR.bits.CLK_SEL = 1;
    IT1.ITPERIOD.data =  (XTI_FREQ/1000)*msec;
    IT1.ITCSR.bits.EN = 1;
}

void timer_IT1_intr_disable()
{
    IT1.ITSCALE.data = 0;
    IT1.ITCSR.bits.INT = 0;
    IT1.ITCSR.bits.EN = 0;
}

void timer_IT1_wait(unsigned int msec)
{

    IT1.ITPERIOD.data =  (XTI_FREQ/1000)*msec;
    IT1.ITSCALE.data = 0;
    IT1.ITCSR.data = (1 << 3) | 1;

    while(!IT1.ITCSR.bits.INT);

    IT1.ITCSR.bits.INT = 0;
}



