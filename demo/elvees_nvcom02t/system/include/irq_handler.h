/**
 * @file:
 * This file implements interrupt handling from QSTR0 for 1892VM10YA  
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef ELVEES_HDR_IRQ_HANDLER_H
#define ELVEES_HDR_IRQ_HANDLER_H


#define IQR_MAX_INTRS 32
/* Interrupt codes for QSTR0 */
enum ID_INTR
{
    ID_INTR_IRQ0,
    ID_INTR_IRQ1,
    ID_INTR_IRQ2,
    ID_INTR_IRQ3,
    ID_INTR_UART0,
    ID_INTR_UART1,
    #define ID_INTR_RSRV0_SIZE 6
    ID_INTR_RSRV00,
    ID_INTR_RSRV01,
    ID_INTR_RSRV02,
    ID_INTR_RSRV03,
    ID_INTR_RSRV04,
    ID_INTR_RSRV05,
    ID_INTR_ETH_RX_FRAME,
    ID_INTR_ETH_TX_FRAME,
    ID_INTR_ETH_DMA_RX,
    ID_INTR_ETH_DMA_TX,
    ID_INTR_VPIN,
    ID_INTR_VPIN_RX,
    ID_INTR_VPOUT,
    ID_INTR_VPOUT_TX,
    ID_INTR_WDT,
    ID_INTR_IRT1,
    ID_INTR_IRT0,
    ID_INTR_INT_I2C,
    ID_INTR_RSRV10,
    ID_INTR_RSRV11,
    ID_INTR_RSRV12,
    ID_INTR_RSRV13,
    ID_INTR_RSRV14,
    ID_INTR_RSRV15,
    ID_INTR_RSRV16,
    #define ID_INTR_RSRV1_SIZE 7
    ID_INTR_INT_MCC
};

/* Table of pointers to the interrupt handler */
struct irq_handlers_table_t
{
    void (*irq_handler_irq0)( );
    void (*irq_handler_irq1)( );
    void (*irq_handler_irq2)( );
    void (*irq_handler_irq3)( );
    void (*irq_handler_uart0)( );
    void (*irq_handler_uart1)( );
    unsigned char rsrv0[ID_INTR_RSRV0_SIZE*sizeof(void (*)( ))];
    void (*irq_handler_eth_rx_frame)( ) ;
    void (*irq_handler_eth_tx_frame)( );
    void (*irq_handler_eth_dma_rx)( );
    void (*irq_handler_eth_dma_tx)( );
    void (*irq_handler_vpin)( );
    void (*irq_handler_vpin_rx)( );
    void (*irq_handler_vpout)( );
    void (*irq_handler_vpout_tx)( );
    void (*irq_handler_wdt)( );
    void (*irq_handler_irt1)( );
    void (*irq_handler_irt0)( );
    void (*irq_handler_int_i2c)( );
    unsigned char rsrv1[ID_INTR_RSRV1_SIZE*sizeof(void (*)( ))];
    void (*irq_handler_int_mcc)( ) ;
};


/**
 * Sets handler for specified interrupt from QSTR0
 * @param intr_id interrupt ID
 * @param handler pointer to the interrupt handler function
 */
void irq_set_handler(enum ID_INTR intr_id, void (*handler)());

#endif
