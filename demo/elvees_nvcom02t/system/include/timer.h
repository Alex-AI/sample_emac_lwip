/**
 * @file:
 * This file implements working with interval timers for program delays and system time on 1892VM10YA chip
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#ifndef ELVEES_HDR_TIMER_H
#define ELVEES_HDR_TIMER_H

#include "nvcom02t.h"

/**
 * Starts counting system time in msecs using the Iterval Timer 0 (IT0)
 * Don't use IT0 anywhere. It's for system time only!
 */
void timer_IT0_sys_time_start();

/**
 * Calculates and returns system time in msec
 * @return value of system time in msec
 */
unsigned int timer_IT0_get_sys_now();

/**
 * Sets timer interrupt using the Iterval Timer 1 (IT1)
 * @param msec waiting time in milliseconds
 */
void timer_IT1_intr(unsigned int msec);

/**
 * Disables timer interrupt using the Iterval Timer 1 (IT1)
 */
void timer_IT1_intr_disable();

/**
 * Causes a blocking wait using the Iterval Timer 1 (IT1)
 * @param msec waiting time in milliseconds
 */
void timer_IT1_wait(unsigned int msec);

#endif /* ELVEES_HDR_TIMER_H */
