/**
 * @file:
 * This file implements some functions for working with CPU of 1892VM10YA
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */


#ifndef ELVEES_HDR_CPU_H
#define ELVEES_HDR_CPU_H

#include "lwipopts.h"

/** 
 * Converting a virtual address to a physical address
 * @param virtaddr virtual address
 */
unsigned int virt_to_phys (void* virtaddr);

/**
 * CPU frequency calculation via PLL and XTI
 * @return CPU frequency
 */
unsigned int cpu_get_freq();  

/**
 * Lists the interrupt sources specified by C0.Status
 */
enum IRQ_SOURCE
{
    IRQ_SOURCE_QSTR0   = (1 << 10),
    IRQ_SOURCE_QSTR1   = (1 << 11),
    IRQ_SOURCE_QSTR2   = (1 << 12),
    IRQ_SOURCE_DSP     = (1 << 14),
    IRQ_SOURCE_COMPARE = (1 << 15)
};

/**
 * Enables interrupt for the C0 coprocessor
 * @param irq_sources interrupt sources
 * @param in_sdram selecting the location of the interrupt vector:
 * in_sdram = 0 - placing a vector in CRAM
 * in_sdram = 1 -placing a vector in SDRAM
 */
void cpu_enable_interrupts(enum IRQ_SOURCE irq_sources, int in_sdram);

/**
 * Disables interrupts for the C0 coprocessor
 * @param irq_sources interrupt sources
 */
void cpu_disable_interrupts(enum IRQ_SOURCE irq_sources);

/**
 * Locks interrupts for the C0 coprocessor
 */
void cpu_lock_interrupts();

/**
 * Unlocks interrupts for the C0 coprocessor
 */
void cpu_unlock_interrupts();

/* C0 coprocessor registers */
#define C0_INDEX                0    
#define C0_RANDOM               1    
#define C0_ENTRYLO0             2    
#define C0_ENTRYLO1             3    
#define C0_CONTEXT              4    
#define C0_PAGEMASK             5    
#define C0_WIRED                6    
#define C0_BADVADDR             8  
#define C0_COUNT                9    
#define C0_ENTRYHI              10  
#define C0_COMPARE              11  
#define C0_STATUS               12   
#define C0_CAUSE                13   
#define C0_EPC                  14   
#define C0_PRID                 15   
#define C0_CONFIG               16   
#define C0_LLADDR               17   
#define C0_ERROREPC             30   

/*
 * Read C0 coprocessor register.
 */
#define mips_read_c0_register(reg)        \
({ int __value;                            \
    asm volatile (                        \
    "mfc0    %0, $%1"                    \
    : "=r" (__value) : "K" (reg));        \
    __value;                            \
})

/*
 * Read C0 coprocessor register with select
 */
#define mips_read_c0_select(reg,sel)                \
    ({ int __value;                                    \
    asm volatile (                                    \
    "mfc0    %0, $%1, %2"                            \
    : "=r" (__value) : "K" (reg), "K" (sel));        \
    __value;                                        \
})

/*
 * Write C0 coprocessor register.
 */
#define mips_write_c0_register(reg, value)                \
    do {                                                \
    asm volatile (                                        \
    "mtc0    %z0, $%1 \n    nop \n    nop \n    nop"            \
    : : "r" ((unsigned int) (value)), "K" (reg));        \
    } while (0)

/*
 * Write C0 coprocessor register with select
 */
#define mips_write_c0_select(reg, sel, value)                   \
do {                                                           \
    asm volatile (                                               \
    "mtc0    %z0, $%1, %2 \n    nop \n    nop \n    nop"               \
    : : "r" ((unsigned int) (value)), "K" (reg), "K" (sel));   \
} while (0)

/*
 * Read FPU (C1 coprocessor) register.
 */
#define mips_read_fpu_register(reg)                \
({ int __value;                                    \
    asm volatile (                                \
    "mfc1    %0, $%1"                            \
    : "=r" (__value) : "K" (reg));                \
    __value;                                    \
})

/*
 * Write FPU (C1 coprocessor) register.
 */
#define mips_write_fpu_register(reg, value)            \
do {                                                \
    asm volatile (                                    \
    "mtc1    %z0, $%1"                                \
    : : "r" (value), "K" (reg));                     \
} while (0)

/*
 * Read FPU control register.
 */
#define mips_read_fpu_control(reg)                \
({ int __value;                                    \
    asm volatile (                                \
    "cfc1    %0, $%1"                            \
    : "=r" (__value) : "K" (reg));                \
    __value;                                    \
})

/*
 * Write FPU control register.
 */
#define mips_write_fpu_control(reg, value)            \
do {                                                \
    asm volatile (                                    \
    "ctc1    %z0, $%1"                                \
    : : "r" ((unsigned int) (value)), "K" (reg));   \
} while (0)



#endif
