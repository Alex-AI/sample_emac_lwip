/**
 * @file:
 * This file implements main hadware initialization and system time 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "lwip/sys.h"
#include "timer.h"
#include "cpu.h"

/**
 * Calculating system time based on interval timer 0 (IT0)
 */
u32_t sys_jiffies()
{
    return timer_IT0_get_sys_now();
}
u32_t sys_now()
{
    return timer_IT0_get_sys_now();
}

/**
 * Hardware initialization:
 * configuring UART, system time, enabling interrupts from QSTR0
 */
void sys_init()
{
    /* Config UART0 for printf */
    uart_config(DEBUG_UART_ID, DEBUG_UART_DEFAULT_BAUDRATE);

    /* Start system timer by IT0 in msec */
    timer_IT0_sys_time_start();

    /* Enables interrupt for the C0 coprocessor */
    cpu_enable_interrupts(IRQ_SOURCE_QSTR0,  CPU_IRQ_VECTOR_IN_SDRAM);

    LWIP_PLATFORM_DIAG(("System initialization has been done\n"));
}
