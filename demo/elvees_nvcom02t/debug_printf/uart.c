/**
 * @file:
 * This file implements the interface to uart port
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "uart.h"
#include "nvcom02t.h"

#define MC_R(a)        *(volatile unsigned*)(0xB82F0000 + (a))

#define MC_RBR(i)                MC_R (0x3000+(i*800))        
#define MC_THR(i)                MC_R (0x3000+(i*800))        
#define MC_IER(i)                MC_R (0x3004+(i*800))        
#define MC_IIR(i)                MC_R (0x3008+(i*800))        
#define MC_FCR(i)                MC_R (0x3008+(i*800))        
#define MC_LCR(i)                MC_R (0x300C+(i*800))        
#define MC_MCR(i)                MC_R (0x3010+(i*800))        
#define MC_LSR(i)                MC_R (0x3014+(i*800))        
#define MC_MSR(i)                MC_R (0x3018+(i*800))        
#define MC_SPR(i)                MC_R (0x301C+(i*800))        
#define MC_DLL(i)                MC_R (0x3000+(i*800))
#define MC_DLM(i)                MC_R (0x3004+(i*800))
#define MC_SCLR(i)               MC_R (0x3014+(i*800))    


//
// Line control register
//
#define MC_LCR_5BITS            0x00    /* character length: 5 bits */
#define MC_LCR_6BITS            0x01    /* character length: 6 bits */
#define MC_LCR_7BITS            0x02    /* character length: 7 bits */
#define MC_LCR_8BITS            0x03    /* character length: 8 bits */

#define MC_LCR_STOPB            0x04    /* use 2 stop bits */
#define MC_LCR_PENAB            0x08    /* parity enable */
#define MC_LCR_PEVEN            0x10    /* even parity */
#define MC_LCR_PFORCE           0x20    /* force parity */
#define MC_LCR_SBREAK           0x40    /* break control */
#define MC_LCR_DLAB             0x80    /* divisor latch access bit */

//
// FIFO control register
//
#define MC_FCR_ENABLE            0x01    /* enable FIFO */
#define MC_FCR_RCV_RST           0x02    /* clear receive FIFO */
#define MC_FCR_XMT_RST           0x04    /* clear transmit FIFO */

#define MC_FCR_TRIGGER_1        0x00    /* receive FIFO level: 1/4 byte */
#define MC_FCR_TRIGGER_4        0x40    /* receive FIFO level: 4/16 bytes */
#define MC_FCR_TRIGGER_8        0x80    /* receive FIFO level: 8/56 bytes */
#define MC_FCR_TRIGGER_14       0xc0    /* receive FIFO level: 14/60 bytes */

//
// Line status register
//
#define MC_LSR_RXRDY            0x01    /* receiver ready */
#define MC_LSR_OE                0x02    /* overrun error */
#define MC_LSR_PE                0x04    /* parity error */
#define MC_LSR_FE                0x08    /* framing error */
#define MC_LSR_BI                0x10    /* break interrupt */
#define MC_LSR_TXRDY            0x20    /* transmitter holding register empty */
#define MC_LSR_TEMT                0x40    /* transmitter empty */
#define MC_LSR_FIFOE            0x80    /* error in receive FIFO */

//
// Interrupt enable register
//
#define MC_IER_ERXRDY            0x01    /* enable receive data/timeout intr */
#define MC_IER_ETXRDY            0x02    /* enable transmitter interrupts */
#define MC_IER_ERLS                0x04    /* enable receive line status intr */
#define MC_IER_EMSC                0x08    /* enable modem status interrupts */

//
// Interrupt identification register
//
#define MC_IIR_NOPEND           0x01        /* no interrupt pending */
#define MC_IIR_IMASK            0x0e        /* interrupt type mask */
#define MC_IIR_FENAB            0xc0    /* set if FIFOs are enabled */

#define MC_IIR_RLS        0x06    /* receiver line status */
#define MC_IIR_RXRDY    0x04    /* receiver data available */
#define MC_IIR_RXTOUT    0x0c    /* character timeout indication */
#define MC_IIR_TXRDY    0x02    /* transmitter holding reg empty */
#define MC_IIR_MLSC        0x00    /* modem status */

//
// Modem control register
//
#define MC_MCR_DTR            0x01    /* control DTR output */
#define MC_MCR_RTS            0x02    /* control RTS output */
#define MC_MCR_OUT1            0x04    /* control OUT1 output */
#define MC_MCR_OUT2            0x08    /* control OUT2 output, used as global interrupt enable in PCs */
#define MC_MCR_LOOPBACK        0x10    /* set local loopback mode */

//
// Modem status register
//
#define MC_MSR_DCTS        0x01    /* CTS changed */
#define MC_MSR_DDSR        0x02    /* DSR changed */
#define MC_MSR_TERI        0x04    /* RI changed from 0 to 1 */
#define MC_MSR_DDCD        0x08    /* DCD changed */
#define MC_MSR_CTS        0x10    /* CTS input */
#define MC_MSR_DSR        0x20    /* DSR input */
#define MC_MSR_RI        0x40    /* RI input */
#define MC_MSR_DCD        0x80    /* DCD input */

//
// Compute the 16-bit baud rate divisor, given
// the oscillator frequency and baud rate.
// Round to the nearest integer.

#define MC_DL_BAUD(fr,bd)    (((fr)/8 + (bd)) / (bd) / 2)

//
// @name: uart_config
// @description: Setup uart port to transfer at given speed and frequency
//
// @in int id   - id of UART (0,1)
// @in unsigned long baud - data transfer speed
//
void uart_config(int id, unsigned long baud)
{
    if ((id < 0)  || (id > 1)) return;
    /* Setup baud rate generator. */
    unsigned divisor = MC_DL_BAUD (cpu_get_freq(), baud);

    MC_LCR(id) = MC_LCR_8BITS | MC_LCR_DLAB;
    MC_DLM(id) = divisor >> 8;
    MC_DLL(id) = divisor;
    MC_LCR(id) = MC_LCR_8BITS;
    MC_SCLR(id) = 0;
    MC_SPR(id) = 0;
    MC_IER(id) = 0;
    MC_MSR(id) = 0;
    MC_MCR(id) = MC_MCR_DTR | MC_MCR_RTS | MC_MCR_OUT2;
    MC_FCR(id) = MC_FCR_RCV_RST | MC_FCR_XMT_RST | MC_FCR_ENABLE;

    /* Clear pending status, data and irq. */

    (void) MC_LSR(id);
    (void) MC_MSR(id);
    (void) MC_RBR(id);
    (void) MC_IIR(id);
}

//
// @name: uart_putchar
// @description: Send symbol
//
// @in int id   - id of UART (0,1)
// @in short c   - symbol
//
void uart_putchar (int id, short c)
{

    if ((id < 0)  || (id > 1)) return;

    for(;;)
    {
        /* Wait for transmitter holding register empty. */
        while (! (MC_LSR(id) & MC_LSR_TXRDY))
            ;

        /* Send byte. */
        MC_THR(id) = c;

        /* Wait for transmitter holding register empty. */
        while (! (MC_LSR(id) & MC_LSR_TXRDY))
            ;

        if (c == '\n')
        {
            c = '\r';
            continue;
        }
        else
        {
            break;
        }

    }

}

//
// @name: uart_getchar
// @description: Wait for the byte to be received and return it.
//
// @in int id   - id of UART (0,1)
//
// @return: unsigned short - received symbol
//
unsigned short uart_getchar (int id)
{
    unsigned char c;

    if ((id < 0)  || (id > 1)) return 0;

    for (;;)
    {
        /* Wait until receive data available. */
        if (! (MC_LSR(id) & MC_LSR_RXRDY))
        {
            continue;
        }
        c = MC_RBR(id);
        break;
    }
    return c;
}

#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include "lwipopts.h"
#undef errno
extern int errno;

int write (int fd, const void *buf, int nbyte )
{
    int i;
    for(i = 0; i < nbyte; i++)
    {
        uart_putchar(DEBUG_UART_ID, ((char*)buf)[i]);
    }

    return nbyte;
}

int  isatty (int file) {
    return 1;
}


int read(int fd, char *buf, int nbyte ) {

    int i;
    for(i = 0; i < nbyte; i++)
    {
        ((char*)buf)[i] = uart_getchar(DEBUG_UART_ID);
    }

    return nbyte;
}

