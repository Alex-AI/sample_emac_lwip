/**
 * @project: 
 * sample_emac_lwip
 * @file:
 * This file implements echo-server demo program
 * This sample is designed for a debug board NVCom-02T
 * All sample settings sets in the header file lwip_options\include\lwipopts.h
 * 
 * The ethernet driver implemenation is located in ethernet_driver\eth.c
 * The packet received by interrupt is placed in the queue
 * 
 * The system\exc_handler.s located at the address of the interrupt vector is responsible for calling the interrupt handler
 * Interrupt handler function is named irq_handler() and located in system\irq_handler.c
 * 
 * The queue is implemented based on a doubly linked list (buf_queue\buf_queue.c)
 * The queue polls in the main program loop
 * When a packet is received from a queue it is passed to the Lwip for processing
 * 
 * Debug output is implemented based on a "write()" system call override
 * 
 * The calculation of the system time is performed through Iterval Timer 0 (IT0) of 1892VM10YA
 * 
 * Functions for working with EMAC of 1892VM10YA-02T are located int ethernet_driver/emac.c
 * 
 * NVCom-02T debug board have Micrel KS8721BL PHY transceiver
 * Functions for working with PHY of NVCom-02T are located int ethernet_driver/phy.c
 * 
 */

/* 
 * Copyright (с) 2020 RnD Center "ELVEES", JSC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Alexander Ionov <alexander.ionov.job@gmail.com>
 *
 */

#include "echo_server.h"

#include "eth.h"
#include "cpu.h"
#include "timer.h"
#include "lwipopts.h"
#include "lwip/sys.h"
#include "lwip/init.h"
#include "lwip/timeouts.h"
#include "lwip/pbuf.h"
#include "lwip/netif.h"
#include "lwip/dhcp.h"
#include "lwip/tcp.h"

/**
 * Callback function called when the netif status changes
 * Displays the IP interface parameters
 * @param netif pointer to the modified netif instance
 */
static void netif_status_callback(struct netif *netif)
{

    LWIP_PLATFORM_DIAG(("Netif status changed...\n"));
    LWIP_PLATFORM_DIAG(("IP addr: %s\n",
            ip4addr_ntoa(netif_ip4_addr(netif)))
            );

    LWIP_PLATFORM_DIAG(("Netmask: %s\n",
            ip4addr_ntoa(netif_ip4_netmask(netif)))
            );

    LWIP_PLATFORM_DIAG(("Gateway: %s\n",
            ip4addr_ntoa(netif_ip4_gw(netif)))
            );
}

/**
 * Outputs debugging information of the received frame
 * Outputs the sequence number of the frame and its size
 * @param p pointer to the pbuf
*/
static unsigned int frame_counter = 0;
static void echo_diag_output(struct pbuf *p)
{
    
    if(p->tot_len > 0)
    {
        frame_counter++;
        LWIP_PLATFORM_DIAG(("Received buf #%u \t p->tot_len = %d\n", frame_counter, p->tot_len));
    }

}

#if USE_TCP_PROTO
/**
 * Callback function called when receiving a frame from TCP.
 * @param arg pointer to additional parameters that are required when calling the callback function
 * @param tpcb pointer to the TCP connection control unit for which the frame was received
 * @param p pointer to the received frame
 * @param err error code, which occurred before calling the callback function
 * @return error code
 */
static err_t tcp_echo_recv_callback(void *arg, struct tcp_pcb *tpcb,
       struct pbuf *p, err_t err)
{
    /* If there is an error before the call, free up memory and exit with the same error */
    if(err != ERR_OK )
    {
        pbuf_free(p);
        return err;
    }
    
    /* If there is no packet, close the connection */
    if(!p)
    {
        return tcp_close(tpcb);
    }

    tcp_recved(tpcb, p->tot_len);

    echo_diag_output(p);

    err = tcp_write(tpcb, p->payload, p->tot_len, TCP_WRITE_FLAG_MORE);
    if(err != ERR_OK)
        return err;

    err = tcp_output(tpcb);

    pbuf_free(p);

    if(err != ERR_OK)
        return err;

    return ERR_OK;
}

/**
 * Callback function called to accept a TCP connection from the client
 * @param arg pointer to additional parameters that are required when calling the callback function
 * @param newpcb pointer to the new TCP connection control unit for the client
 * @param err error code, which occurred before calling the callback function
 * @return error code
 */
static err_t tcp_echo_accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{
    if(err != ERR_OK)
        return err;

    /* Set a handler for the newpcb client */
    tcp_recv(newpcb, tcp_echo_recv_callback);

    return ERR_OK;
}
#endif


#if USE_UDP_PROTO
/**
 * Callback function called when receiving a packet
 * @param arg pointer to additional parameters that are required when calling the callback function
 * @param newpcb pointer to the UDP connection control unit for which the frame was received
 * @param addr pointer to the sender's address
 * @param port sender's port number
*/
static void udp_echo_recv_callback(void *arg, struct udp_pcb *pcb, struct pbuf *p,
    const ip_addr_t *addr, u16_t port)
{

    if(!p)
    {
        LWIP_PLATFORM_DIAG((" echo - pbuf: null pointer \n"));
        return;
    }

    echo_diag_output(p);

    err_t err = udp_sendto(pcb, p, addr, port);
    if(err != ERR_OK)
    {
        LWIP_PLATFORM_DIAG((" echo - udp_sendto(): some error occurred \n"));
    }

    pbuf_free(p);
}
#endif



static struct netif netif;

/** 
 * Main operating loop of the program
 */
void echo_server_mainloop()
{
    /* Initializing the lwIP core and system */
    lwip_init(&netif);
    

    /* Creating the netif interface without specifying IP parameters */
    netif_add_noaddr(&netif, NULL, netif_init_eth, netif_input);

    /* Configuring the netif interface */
    netif.name[0] = 'e';
    netif.name[1] = '0';
    netif_create_ip6_linklocal_address(&netif, 1);
    netif.ip6_autoconfig_enabled = 1;
    netif_set_default(&netif);
    netif_set_up(&netif);
    netif_set_status_callback(&netif, netif_status_callback);
    LWIP_PLATFORM_DIAG(("lwIP layer initialization has been done\n"));

#if USE_STATIC_IP
    ip_addr_t ip4_src_addr, netmask, gateway;
    ipaddr_aton(DEFAULT_IP, &ip4_src_addr);
    ipaddr_aton(DEFAULT_NETMASK, &netmask);
    ipaddr_aton(DEFAULT_GATEWAY, &gateway);
    netif_set_addr(netif, &ip4_src_addr, &netmask, &gateway);
    LWIP_PLATFORM_DIAG(("Static IP configuration has been used\n"));
#else
    /* Starting DHCP */
    dhcp_start(&netif);
    LWIP_PLATFORM_DIAG(("DHCP configuration has been enabled\n"));
#endif

#if USE_UDP_PROTO
    /* Configuring UDP PCB for echo server implementation */
    struct udp_pcb* echo_pcb = udp_new();
    udp_bind(echo_pcb, IP_ADDR_ANY, ECHO_SERVER_LISTEN_PORT);
    udp_recv(echo_pcb, udp_echo_recv_callback, NULL);
    LWIP_PLATFORM_DIAG(("UDP echo server has been launched\n"));
#endif

#if USE_TCP_PROTO
    /* Configuring TCP PCB for echo server implementation */
    struct tcp_pcb* echo_pcb = tcp_new();
    tcp_bind(echo_pcb, IP_ADDR_ANY, ECHO_SERVER_LISTEN_PORT);
    echo_pcb = tcp_listen(echo_pcb);
    tcp_accept(echo_pcb, tcp_echo_accept_callback );
    LWIP_PLATFORM_DIAG(("TCP echo server has been launched\n"));
#endif


    /* Next lwip core timeout */
    unsigned int next_timeout;
    /* Server duty cycle */
    while(1) {

        /* Checking for a connection to the physical layer */
        if(eth_link_state_changed()) {
            if(eth_link_is_up())
            {
                LWIP_PLATFORM_DIAG(("Link is up\n"));
                netif_set_link_up(&netif);
            }
            else
            {
                LWIP_PLATFORM_DIAG(("Link is down\n"));
                netif_set_link_down(&netif);
            }
        }


        /* Getting a frame from the Ethernet driver receiving queue */
        cpu_lock_interrupts();
        struct pbuf* p = eth_try_get_rx_buf();
        cpu_unlock_interrupts();

        /* If there is a package, it is sent to the lwIP core for processing */
        if(p != NULL) {

            LINK_STATS_INC(link.recv);
            if(netif.input(p, &netif) != ERR_OK)
            {
                pbuf_free(p);
            }
        }

        /**
         * Waiting for the next lwIP timeout
         * Waiting is Limited using the MAX_MAINLOOPTIMER_DELAY macro
         */
        //TODO: try via INT1 interrupt
        next_timeout = sys_timeouts_sleeptime();
        timer_IT1_wait((next_timeout < MAX_MAINLOOP_TIMER_DELAY) ?
                        next_timeout :
                        MAX_MAINLOOP_TIMER_DELAY);
        sys_check_timeouts();

    }
}
